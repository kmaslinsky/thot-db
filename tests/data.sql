PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
INSERT INTO user (fullname, email, username, password)
VALUES
  ('Test', 'test@example.com', 'test', 'pbkdf2:sha256:50000$TCI4GzcX$0de171a4f4dac32e3364c7ddc7c14f3e2fa61f2d17574483f7ffbb431b4acb2f'),
  ('Other', 'other@example.com', 'other', 'pbkdf2:sha256:50000$kJPKsz6N$d2d4784f1b030a9761f5ccaeeaca413f27f2ecb76d6168407af962ddce849f79');
INSERT INTO descriptions VALUES(1,579,'Valentin Vydrin',2,'2024-02-03 10:04:25',1);
COMMIT;
