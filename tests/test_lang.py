import pytest
from thotdb.db import get_db


def test_index(client, auth):
    response = client.get('/')
    assert b"Log In" in response.data
    assert b"Register" in response.data

    auth.login()
    response = client.get('/')
    assert b'Log Out' in response.data
    assert b'Valentin Vydrin' in response.data
    assert b'Bambara' in response.data
    assert b'href="/579/update"' in response.data


@pytest.mark.parametrize('path', (
    '/create',
    '/1/update',
    '/1/delete',
))
def test_login_required(client, path):
    response = client.post(path)
    assert response.headers["Location"] == "/auth/login"


@pytest.mark.parametrize('path', (
    '/2/update',
    '/2/delete',
))
def test_exists_required(client, auth, path):
    auth.login()
    assert client.post(path).status_code == 404


def test_create(client, auth, app):
    auth.login()
    assert client.get('/create').status_code == 200
    client.post('/create', data={'lang_id': '111', 'author': 'test Author'})

    with app.app_context():
        db = get_db()
        count = db.execute('SELECT COUNT(id) FROM descriptions').fetchone()[0]
        assert count == 2


def test_update(client, auth, app):
    auth.login()
    assert client.get('/579/update').status_code == 200
    client.post('/579/update', data={'author': 'new test Author'})

    with app.app_context():
        db = get_db()
        desc = db.execute('SELECT * FROM descriptions WHERE lang_id = 579').fetchone()
        assert desc['author'] == 'new test Author'


@pytest.mark.parametrize('path', (
    '/create',
))
def test_create_validate(client, auth, path):
    auth.login()
    response = client.post(path, data={'lang_id': '', 'author': ''})
    assert b'Lang_id is required.' in response.data


def test_delete(client, auth, app):
    auth.login()
    response = client.post('/579/delete')
    assert response.headers["Location"] == "/"

    with app.app_context():
        db = get_db()
        desc = db.execute('SELECT * FROM descriptions WHERE id = 579').fetchone()
        assert desc is None
