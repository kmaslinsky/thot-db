"""maps cache table

Revision ID: 4eaf0217d336
Revises: 847390d0217a
Create Date: 2024-09-17 15:30:03.087589

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '4eaf0217d336'
down_revision: Union[str, None] = '847390d0217a'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('maps_cache',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('page', sa.String(), nullable=False),
    sa.Column('selector', sa.String(), nullable=True),
    sa.Column('mapframe', sa.String(), nullable=True),
    sa.Column('invalid', sa.Boolean(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('maps_cache')
    # ### end Alembic commands ###
