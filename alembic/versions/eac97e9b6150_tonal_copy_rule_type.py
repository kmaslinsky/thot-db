"""tonal copy rule type

Revision ID: eac97e9b6150
Revises: 9dae5bcb7c9e
Create Date: 2024-09-12 15:56:12.149366

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = 'eac97e9b6150'
down_revision: Union[str, None] = '9dae5bcb7c9e'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.execute("INSERT INTO rule_types(name) VALUES ('toneme copy')")


def downgrade() -> None:
    op.execute("DELETE FROM rule_types WHERE name == 'toneme copy'")
