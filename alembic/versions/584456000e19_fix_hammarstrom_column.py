"""fix hammarstrom column

Revision ID: 584456000e19
Revises: d05d4962c3e7
Create Date: 2024-07-25 18:27:55.979553

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '584456000e19'
down_revision: Union[str, None] = 'd05d4962c3e7'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.execute('UPDATE tonal_status SET hammarstrom = CASE WHEN hammarstrom == 1 THEN 2 WHEN hammarstrom == 0 THEN 3 END')


def downgrade() -> None:
    op.execute('UPDATE tonal_status SET hammarstrom = CASE WHEN hammarstrom == 2 THEN 1 WHEN hammarstrom == 3 THEN 0 END')
