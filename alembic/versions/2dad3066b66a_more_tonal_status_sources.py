"""more tonal status sources

Revision ID: 2dad3066b66a
Revises: 492a3914fce1
Create Date: 2024-09-06 14:20:40.053502

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa
from sqlalchemy.orm.session import Session
from thotdb.models import TonalStatus
# from sqlalchemy.orm import DeclarativeBase
# from sqlalchemy.orm import Mapped, mapped_column
# from sqlalchemy import ForeignKey
# from sqlalchemy.orm import registry
# from flask_sqlalchemy import SQLAlchemy
from typing import Optional
import csv


# revision identifiers, used by Alembic.
revision: str = '2dad3066b66a'
down_revision: Union[str, None] = '492a3914fce1'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None

# class_registry = {}
# reg = registry(class_registry=class_registry)


# class Base(DeclarativeBase):
#     registry = reg


# db = SQLAlchemy(model_class=Base)


# class TonalStatus(db.Model):
#     lang_id: Mapped[str] = mapped_column(ForeignKey("lang_info.lang_id"), primary_key=True)
#     tonal: Mapped[Optional[int]] = mapped_column(ForeignKey("info_status.id"))
#     checked: Mapped[Optional[str]]
#     hyman: Mapped[Optional[int]]
#     lapsyd: Mapped[Optional[str]]
#     wals: Mapped[Optional[str]]
#     hammarstrom: Mapped[Optional[int]]
#     thot: Mapped[Optional[int]]
#     comment: Mapped[Optional[str]]
#     data_status_id: Mapped[Optional[int]] = mapped_column(ForeignKey("data_status.id"))
#     data_comment: Mapped[Optional[str]]


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table("tonal_status") as batch_op:
        batch_op.add_column(sa.Column('wals', sa.String(), nullable=True))
        batch_op.alter_column('lapsyd', existing_type=sa.INTEGER(),
                              type_=sa.String(), existing_nullable=True)
    # ### end Alembic commands ###
    session = Session(bind=op.get_bind())
    with open('thotdb/data/tonal_status_sources.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            lang_id = row.pop('lang_id')
            ts = session.query(TonalStatus).get(lang_id)
            if not ts:
                ts = TonalStatus(lang_id=lang_id)
            for key, value in row.items():
                try:
                    setattr(ts, key, value)
                except AttributeError:
                    print(lang_id)
            session.add(ts)
    session.commit()


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table("tonal_status") as batch_op:
        batch_op.alter_column('lapsyd', existing_type=sa.String(),
                              type_=sa.INTEGER(), existing_nullable=True)
        batch_op.drop_column('wals')
    # ### end Alembic commands ###
