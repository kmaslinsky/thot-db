"""extend tonal levels

Revision ID: 04e0b77b4e13
Revises: d50e20d0b5ec
Create Date: 2024-10-21 13:48:36.624869

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa
from thotdb.models import TonalLevels, tonal_levels_list

# revision identifiers, used by Alembic.
revision: str = '04e0b77b4e13'
down_revision: Union[str, None] = 'd50e20d0b5ec'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    tonal_levels = TonalLevels.__table__

    op.bulk_insert(
        tonal_levels,
        [
            {"id": 6, "name": 4},
            {"id": 7, "name": 4}
        ]
    )

    op.bulk_insert(
        tonal_levels_list,
        [
            {"num": 6, "tlevel": 3},
            {"num": 6, "tlevel": 4},
            {"num": 6, "tlevel": 5},
            {"num": 6, "tlevel": 6},
            {"num": 7, "tlevel": 2},
            {"num": 7, "tlevel": 3},
            {"num": 7, "tlevel": 4},
            {"num": 7, "tlevel": 5},
        ]
    )


def downgrade() -> None:
    pass
