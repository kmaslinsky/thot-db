"""tonal_status cleanup empty strings

Revision ID: b59111e03277
Revises: bf8f4a088e29
Create Date: 2024-09-30 16:17:20.718012

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = 'b59111e03277'
down_revision: Union[str, None] = 'bf8f4a088e29'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.execute("UPDATE tonal_status SET lapsyd = NULL WHERE lapsyd == ''")
    op.execute("UPDATE tonal_status SET wals = NULL WHERE wals == ''")


def downgrade() -> None:
    pass
