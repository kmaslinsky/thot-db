"""extend tonal status table

Revision ID: b3620e8ccec9
Revises: 
Create Date: 2024-07-11 12:30:59.138772

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = 'b3620e8ccec9'
down_revision: Union[str, None] = None
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    with op.batch_alter_table("tonal_status") as batch_op:
        batch_op.add_column(sa.Column("lapsyd", sa.Integer(), nullable=True))
        batch_op.add_column(sa.Column("hammarstrom", sa.Integer(), nullable=True))
        batch_op.add_column(sa.Column("thot", sa.Integer(), nullable=True))
        batch_op.add_column(sa.Column("comment", sa.String(), nullable=True))

    op.execute('UPDATE tonal_status SET hammarstrom = tonal')
    op.execute("UPDATE tonal_status SET thot = CASE WHEN checked LIKE 'DG%' THEN 1 ELSE 0 END")

def downgrade() -> None:
    pass
