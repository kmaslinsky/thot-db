"""v0.5 tonal status extended

Revision ID: d05d4962c3e7
Revises: d98f3fc776eb
Create Date: 2024-07-25 17:27:49.280524

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = 'd05d4962c3e7'
down_revision: Union[str, None] = 'd98f3fc776eb'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    data_status = op.create_table('data_status',
                                  sa.Column('id', sa.Integer(), primary_key=True),
                                  sa.Column('name', sa.String(), nullable=False),
                                  sa.PrimaryKeyConstraint('id'),
                                  sa.UniqueConstraint('name')
                                  )
    op.bulk_insert(
        data_status,
        [
            {"id": 1, "name": "sufficient"},
            {"id": 2, "name": "likely sufficient"},
            {"id": 3, "name": "likely insufficient"},
            {"id": 4, "name": "insufficient"},
            {"id": 5, "name": "yet unclear"},
        ]
    )
    with op.batch_alter_table("tonal_status") as batch_op:
        batch_op.add_column(sa.Column('data_status_id', sa.Integer(), nullable=True))
        batch_op.add_column(sa.Column('data_comment', sa.String(), nullable=True))
        batch_op.create_foreign_key('fk_data_status', 'data_status', ['data_status_id'], ['id'])


def downgrade() -> None:
    with op.batch_alter_table("tonal_status") as batch_op:
        batch_op.drop_constraint('fk_data_status', type_='foreignkey')
        batch_op.drop_column('data_comment')
        batch_op.drop_column('data_status_id')
    op.drop_table('data_status')
