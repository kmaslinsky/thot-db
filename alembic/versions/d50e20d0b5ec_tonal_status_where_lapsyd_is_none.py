"""tonal status where lapsyd is none

Revision ID: d50e20d0b5ec
Revises: 392a84aee2cd
Create Date: 2024-10-02 15:25:01.286030

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa
from thotdb.models import Quotas


# revision identifiers, used by Alembic.
revision: str = 'd50e20d0b5ec'
down_revision: Union[str, None] = '392a84aee2cd'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.execute("UPDATE tonal_status SET tonal = 3 WHERE tonal == 2 AND checked is null AND lapsyd LIKE 'N%' AND (wals LIKE 'N%' or WALS is null)")
    Quotas.update_all()


def downgrade() -> None:
    op.execute("UPDATE tonal_status SET tonal = 2 WHERE tonal == 3 AND checked is null AND lapsyd LIKE 'N%' AND (wals LIKE 'N%' or WALS is null)")
