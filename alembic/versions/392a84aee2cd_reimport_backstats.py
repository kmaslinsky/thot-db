"""reimport backstats

Revision ID: 392a84aee2cd
Revises: b59111e03277
Create Date: 2024-09-30 16:51:46.695422

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa
from sqlalchemy.orm.session import Session
import csv
import datetime
from thotdb.models import TonalStatusStats


# revision identifiers, used by Alembic.
revision: str = '392a84aee2cd'
down_revision: Union[str, None] = 'b59111e03277'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.execute("DELETE FROM tonal_status_stats")
    session = Session(bind=op.get_bind())
    with open('thotdb/data/backstats.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            row['date'] = datetime.datetime.strptime(row['date'], "%Y%m%d").date()
            ts = TonalStatusStats(**row)
            session.add(ts)
        session.commit()
    


def downgrade() -> None:
    pass
