"""rule dependencies

Revision ID: b364b73801f1
Revises: eac97e9b6150
Create Date: 2024-09-13 12:11:32.912689

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = 'b364b73801f1'
down_revision: Union[str, None] = 'eac97e9b6150'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('rule_dependencies',
    sa.Column('rule_id', sa.Integer(), nullable=False),
    sa.Column('depends_on', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['depends_on'], ['tonal_rules.rule_id'], ),
    sa.ForeignKeyConstraint(['rule_id'], ['tonal_rules.rule_id'], ),
    sa.PrimaryKeyConstraint('rule_id', 'depends_on')
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('rule_dependencies')
    # ### end Alembic commands ###
