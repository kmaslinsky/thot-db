import wtforms
from wtforms import validators
from wtforms_sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from thotdb.models import InfoStatus
from markupsafe import Markup


def info_status():
    return InfoStatus.query.where(InfoStatus.id > 0)


def get_info_status(id):
    return InfoStatus.query.get(id)


def get_form_info_status(data):
    try:
        return int(data.id)
    except AttributeError:
        return 0


class BootstrapListWidget(wtforms.widgets.ListWidget):
 
    def __call__(self, field, **kwargs):
        kwargs.setdefault("id", field.id)
        html = [f"<{self.html_tag} {wtforms.widgets.html_params(**kwargs)}>"]
        for subfield in field:
            if self.prefix_label:
                html.append(f"<li class='list-group-item'>{subfield.label} {subfield(class_='form-check-input ms-1')}</li>")
            else:
                html.append(f"<li class='list-group-item'>{subfield(class_='form-check-input me-1')} {subfield.label}</li>")
        html.append("</%s>" % self.html_tag)
        return Markup("".join(html))


class RadioListWidget(wtforms.widgets.ListWidget):
 
    def __call__(self, field, **kwargs):
        kwargs.setdefault("id", field.id)
        html = [f"<{self.html_tag} {wtforms.widgets.html_params(**kwargs)}>"]
        for subfield in field:
            html.append(f"<div class='form-check'>{subfield(class_='form-check-input me-1', type='radio')} {subfield.label}</div>")
        html.append("</%s>" % self.html_tag)
        return Markup("".join(html))


class MultiCheckboxField(QuerySelectMultipleField):
    """
    A multiple-select, except displays a list of checkboxes.

    Iterating the field will produce subfields, allowing custom rendering of
    the enclosed checkbox fields.
    """

    widget = BootstrapListWidget(prefix_label=False)
    option_widget = wtforms.widgets.CheckboxInput()


class MultiRadioField(QuerySelectMultipleField):
    """
    A multiple-select, except displays a list of checkboxes.

    Iterating the field will produce subfields, allowing custom rendering of
    the enclosed checkbox fields.
    """

    widget = RadioListWidget(prefix_label=False)
    option_widget = wtforms.widgets.RadioInput()


class InfoStatusField(QuerySelectField):
    """
    An extended boolean field resulting in InfoStatus object
    """
    def __init__(self, **kwargs):
        super(InfoStatusField, self).__init__(
            query_factory=info_status,
            allow_blank=True,
            blank_value='0',
            validators=[validators.Optional()], **kwargs)


