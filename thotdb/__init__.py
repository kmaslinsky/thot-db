import os
from flask import Flask
from werkzeug.middleware.proxy_fix import ProxyFix
# from werkzeug.middleware.profiler import ProfilerMiddleware


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        SQLALCHEMY_DATABASE_URI = 'sqlite:///thotdb.sqlite',
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('thotdb.cfg', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # a simple page that says hello
    @app.route('/hello')
    def hello():
        return 'Hello, Thot!'

    from . import db
    db.init_app(app)

    from . import auth
    app.register_blueprint(auth.bp)

    from . import language
    from . import theory
    from . import reports
    app.register_blueprint(language.bp)
    app.register_blueprint(theory.bp)
    app.register_blueprint(reports.bp)
    app.register_blueprint(maps.bp)
    app.add_url_rule('/', endpoint='reports.dashboard')

    app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_host=1, x_proto=1, x_prefix=1)
    # app.wsgi_app = ProfilerMiddleware(app.wsgi_app, restrictions=('reports.py|models.py|folium.*', .5))
    
    return app
