def ntonal(taxon):
    tonal = 0
    nontonal = 0
    for lang in taxon.languages:
        if lang.is_tonal:
            tonal += 1
        elif lang.is_nontonal:
            nontonal += 1
    return (tonal, nontonal)


def ncandidates(taxon):
    tonal, _ = ntonal(taxon)
    for lang in taxon.languages:
        if lang.is_tonal and not lang.is_candidate:
            tonal -= 1
    return tonal


def estimate_ntonal(taxon):
    tonal, nontonal = ntonal(taxon)
    if tonal + nontonal == 0:
        return 0
    total = len(taxon.languages)
    estimate = int(round(total * (tonal / (tonal + nontonal)), 0))
    return estimate


class TaxonStats(object):
    def __init__(self):
        self.tonal = 0
        self.toneless = 0
        self.unknown = 0

    def __add__(self, other):
        self.tonal += other.tonal
        self.toneless += other.toneless
        self.unknown += other.unknown
        return self

    def __repr__(self):
        return f'<TaxonStats tonal: {self.tonal}, toneless: {self.toneless}, unknown: {self.unknown}>'

    def __str__(self):
        return f'<TaxonStats tonal: {self.tonal}, toneless: {self.toneless}, unknown: {self.unknown}>'


class GlobalStats(object):
    def __init__(self, total=0, tonal=0, toneless=0, unknown=0, checked=0, estimate=0, quota=0):
        self.total = total
        self.tonal = tonal
        self.toneless = toneless
        self.unknown = unknown
        self.checked = checked
        self.estimate = estimate
        self.quota = quota

    def __sub__(self, other):
        result = GlobalStats()
        result.total = self.total - other.total
        result.tonal = self.tonal - other.tonal
        result.toneless = self.toneless - other.toneless
        result.unknown = self.unknown - other.unknown
        result.checked = self.checked - other.checked
        result.estimate = self.estimate - other.estimate
        result.quota = self.quota - other.quota
        return result

    def __repr__(self):
        return f'<GlobalStats: total: {self.total}, tonal: {self.tonal}, toneless: {self.toneless}, unknown: {self.unknown}, checked: {self.checked}, estimate: {self.estimate}, quota: {self.quota}>'

    def __str__(self):
        return f'<GlobalStats: total: {self.total}, tonal: {self.tonal}, toneless: {self.toneless}, unknown: {self.unknown}, checked: {self.checked}, estimate: {self.estimate}, quota: {self.quota}>'
