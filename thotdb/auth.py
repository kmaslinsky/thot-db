import functools
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for, current_app
)
from werkzeug.security import check_password_hash, generate_password_hash
from thotdb.models import db
from sqlalchemy.sql import text
from thotdb.models import User
from sqlalchemy.exc import IntegrityError
from flask_wtf import FlaskForm
from wtforms import PasswordField, EmailField, SubmitField, StringField
from wtforms.validators import DataRequired, EqualTo, Length, AnyOf
from wtforms import ValidationError

bp = Blueprint('auth', __name__, url_prefix='/auth')


class RegisterForm(FlaskForm):
    fullname = StringField(label='Full name',
                           validators=[DataRequired(), Length(min=3, max=64)])
    email = EmailField(label='Email',
                       validators=[DataRequired()])
    username = StringField(label='Username',
                           validators=[DataRequired(), Length(min=3, max=32)])
    password = PasswordField('Password',
            validators=[DataRequired(), Length(min=6, max=64)])
    confirm = PasswordField('Verify password',
            validators=[DataRequired(), EqualTo('password',
            message='Passwords must match')])
    submit = SubmitField('Register')


class LoginForm(FlaskForm):
    username = StringField(label='Username',
                           validators=[DataRequired(), Length(min=3, max=32)])
    password = PasswordField(label='Password',
                             validators=[DataRequired(), Length(min=6, max=64)])
    submit = SubmitField('Log in')

    
@bp.route('/register', methods=('GET', 'POST'))
def register():
    form = RegisterForm()
    form.email.validators.append(AnyOf(current_app.config['EMAILS_ALLOWED'],
                                       message='User is not in a list of allowed users. Please contact the project team'))

    if form.validate_on_submit():
        user = User(
            fullname=form.fullname.data,
            email=form.email.data,
            username=form.username.data,
            password=generate_password_hash(form.password.data)
            )
        try:
            db.session.add(user)
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            flash(f"User {user.username} is already registered or {user.email} is already used.", 'danger')
        else:
            return redirect(url_for("auth.login"))    
    elif request.method == 'POST':
        for key, message in form.errors.items():
            flash(f'Error: {message[0]}', 'warning')

    return render_template('auth/register.html', form=form)


@bp.route('/login', methods=('GET', 'POST'))
def login():
    form = LoginForm()

    if form.validate_on_submit():
        user = db.session.execute(
            db.select(User).where(User.username == form.username.data)
        ).scalar()
        if user is None:
            flash('Incorrect username.', 'warning')
        elif not check_password_hash(user.password, form.password.data):
            flash('Incorrect password.', 'warning')
        elif user.email not in current_app.config['EMAILS_ALLOWED']:
            flash('User account is currently disabled. Please contact ThoT project team.', 'danger')
        else:
            try:
                url = session['url']
            except KeyError:
                url = None
            session.clear()
            session['user_id'] = user.id
            if url:
                return redirect(url)
            return redirect(url_for('reports.dashboard'))
    
    elif request.method == 'POST':
        for key, message in form.errors.items():
            flash(f'Error: {message[0]}')

    return render_template('auth/login.html', form=form)


@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')

    if user_id is None:
        g.user = None
    else:
        g.user = db.session.execute(db.select(User).where(User.id == user_id)).scalar()


@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('reports.dashboard'))


def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapped_view
