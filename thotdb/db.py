import click
import csv
import os
from flask.cli import with_appcontext
from flask import current_app
from sqlalchemy import inspect, and_
from sqlalchemy.orm import aliased
from thotdb.models import db, class_registry
from thotdb.models import (LangInfo, ThotLangInfo, Taxons,
                           TonalSystem, TdMinsize, Boundaries,
                           LevelsInventory, TonalLevels,
                           TonemesInventory, TonemesComposition,
                           Allotones, AllotonesComposition,
                           CriterionType, TonemeCriteria,
                           TonemeStatus, NonpitchFeatures,
                           NonpitchComponents, SurfaceTones,
                           NonlevelComponents, PhoneticBoundaries,
                           TonalRulesBoundaries, DescStatus, User,
                           Descriptions, Quotas, TonalStatus)
from thotdb.models import TbuUnits, tonal_levels_list, td_boundaries
from thotdb.models import taxon_langs, dialects, leaf_taxons
from thotdb.stats import ntonal, ncandidates, estimate_ntonal
from thotdb.maps import map_world, invalidate_cache
from thotdb import create_app


def get_db():
    return db.session


def clear_table(obj):
    inspector = inspect(db.engine)
    try:
        table = obj.__table__
        tabname = obj.__tablename__
    except AttributeError:
        table = obj
        tabname = obj.name
    if inspector.has_table(tabname):
        table.drop(db.engine)
        table.create(db.engine)


def import_csv_table(app_root, path, obj, keepcols=None):
    clear_table(obj)
    path = os.path.join(app_root, path)
    with open(path) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            for k, v in row.items():
                if v in ('', 'n/d'):
                    row[k] = None
            if keepcols is None:
                item = row
            else:
                item = {newkey: row[key] for key, newkey in keepcols.items()}
            li = obj(**item)
            db.session.add(li)
        db.session.commit()


def import_glottolog_data(app_root):
    import_csv_table(app_root, "data/lang_info.csv", LangInfo)
    import_csv_table(app_root, "data/taxons.csv", Taxons)


def import_quotas(app_root):
    import_csv_table(app_root, "data/quotas.csv", Quotas,
                     keepcols={
                         'glottocode': 'taxon_id',
                         'tonal': 'tonal',
                         'nontonal': 'nontonal',
                         'totaltonal': 'totaltonal',
                         'quota': 'quota'
                     })


def import_tonal_status(app_root):
    import_csv_table(app_root, "data/tonal_status.csv", TonalStatus)


def create_enums(app_root):
    path = os.path.join(app_root, "data/enums.csv")
    with open(path) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            obj = class_registry[row['model']]
            if db.session.query(obj).filter(obj.name == row['name']).first() is not None:
                continue
            else:
                params = {'name': row['name']}
                if row['key']:
                    params[row['key']] = row['value']
                li = obj(**params)
                db.session.add(li)
        db.session.commit()


def set_tonal_levels_inventory():
    tlmap = {
        '2': ['H', 'L'],
        '3': ['H', 'M', 'L'],
        '4': ['xH', 'H', 'L', 'xL'],
        '5': ['xH', 'H', 'M', 'L', 'xL'],
        '6': ['xxH', 'xH', 'H', 'L', 'xL', 'xxL']
    }
    nonlevel = {
        'D': 'downstep'
    }
    for nlevels, llist in tlmap.items():
        tl = TonalLevels.query.filter(TonalLevels.name == nlevels).one()
        for lname in llist:
            li = LevelsInventory.query.filter(LevelsInventory.name == lname).one()
            tl.levels.append(li)
        db.session.add(tl)
    for compname, desc in nonlevel.items():
        comp = LevelsInventory.query.filter(LevelsInventory.name == compname).one()
        nl = NonlevelComponents(id=comp.id, description=desc)
        db.session.add(nl)
    db.session.commit()


def subfamilies(taxon):
    taxonalias = aliased(Taxons)
    subfamilies = db.session.query(Taxons).filter(Taxons.taxon_id == taxon.taxon_id).cte(name="subfamilies", recursive=True)
    subfamilies_a = aliased(subfamilies, name="parent")
    subfamilies = subfamilies.union(
        db.session.query(taxonalias).join(subfamilies_a, taxonalias.parent)
    )
    res = db.session.query(subfamilies)
    return res


def languages(taxon):
    parents = [t.taxon_id for t in subfamilies(taxon)]
    langs = LangInfo.query.filter(and_(LangInfo.parent_id.in_(parents), LangInfo.level == 'language')).order_by(LangInfo.name).all()
    if not langs:
        langs = [LangInfo.query.get(taxon.taxon_id)]
    return langs


def cache_taxon_languages():
    clear_table(taxon_langs)
    taxons = Quotas.query.all()
    for qtaxon in taxons:
        taxon = qtaxon.taxon
        langs = languages(taxon)
        if langs:
            drows = [{'taxon_id': taxon.taxon_id, 'lang_id': l.lang_id} for l in langs]
            ins = taxon_langs.insert()
            db.session.execute(ins, drows)
    db.session.commit()


def cache_leaf_taxons():
    clear_table(leaf_taxons)
    taxons = Taxons.query.all()
    for taxon in taxons:
        if taxon.child_language_count == 0:
            langshere = [LangInfo.query.get(taxon.taxon_id)]
        else:
            langshere = LangInfo.query.filter(LangInfo.parent_id == taxon.taxon_id).all()
        if langshere:
            try:
                drows = [{'taxon_id': taxon.taxon_id, 'lang_id': l.lang_id} for l in langshere]
            except AttributeError:
                print(taxon, langshere)
            ins = leaf_taxons.insert()
            db.session.execute(ins, drows)
    db.session.commit()


def list_dialects(lang):
    langalias = aliased(LangInfo)
    dialects = db.session.query(LangInfo).filter(and_(LangInfo.level == 'dialect', LangInfo.parent_id == lang.lang_id)).cte(name="dialects", recursive=True)
    dialects_a = aliased(dialects, name="lang_parent")
    dialects = dialects.union(
        db.session.query(langalias).join(dialects_a, langalias.lang_parent)
    )
    res = [LangInfo.query.get(r[0]) for r in db.session.query(dialects).all()]
    return res


def cache_dialects():
    clear_table(dialects)
    langs = LangInfo.query.filter(LangInfo.level == 'language').all()
    for lang in langs:
        ds = list_dialects(lang)
        if ds:
            drows = [{'lang_id': lang.lang_id, 'dialect_id': d.lang_id} for d in ds]
            ins = dialects.insert()
            db.session.execute(ins, drows)
            db.session.commit()


def cache_ntonal():
    quotas = Quotas.query.all()
    for quota in quotas:
        taxon = quota.taxon
        if not taxon:
            print(quota)
        else:
            quota.update_stats()


def cache_world_maps():
    app = create_app()
    with app.app_context():
        invalidate_cache('map_world')
        invalidate_cache('map_family')
        map_world('tonal', max_zoom=10)
        map_world('unknown|toneless|tonal', max_zoom=10)
        print('World maps cached')


def import_data(app_root):
    import_glottolog_data(app_root)
    import_quotas(app_root)
    cache_taxon_languages()
    cache_leaf_taxons()
    cache_ntonal()
    cache_dialects()
    cache_world_maps()


def init_db(app_root):
    db.create_all()
    import_data(app_root)
    create_enums(app_root)
    set_tonal_levels_inventory()


def create_tables():
    db.create_all()
    

@click.command('clear-db')
def clear_db_command():
    """Drop all tables with the data"""
    db.reflect()
    db.drop_all()


@click.command('init-db')
@with_appcontext
def init_db_command():
    """Clear the existing data and create new tables"""
    app_root = current_app.root_path
    init_db(app_root)
    click.echo('Initialized the database')


@click.command('import-data')
@with_appcontext
def import_data_command():
    """Import extarnal data (glottolog and quotas)"""
    app_root = current_app.root_path
    import_data(app_root)
    click.echo('Data imported successfully')


@click.command('import-glottolog-data')
@with_appcontext
def import_glottolog_command():
    """Import extarnal data (glottolog and quotas)"""
    app_root = current_app.root_path
    import_glottolog_data(app_root)
    click.echo('Glottolog data imported successfully')


@click.command('create-tables')
def create_tables_command():
    app_root = current_app.root_path
    create_tables()
    create_enums(app_root)
    click.echo('New tables and enums created successfully')


@click.command('cache-taxons')
def cache_taxons_command():
    cache_taxon_languages()
    cache_leaf_taxons()


@click.command('cache-ntonal')
def cache_ntonal_command():
    cache_ntonal()


@click.command('cache-dialects')
def cache_dialects_command():
    cache_dialects()


@click.command('cache-maps')
def cache_maps_command():
    cache_world_maps()
    click.echo('World maps cached successfully')


def init_app(app):
    db.init_app(app)
    app.cli.add_command(init_db_command)
    app.cli.add_command(clear_db_command)
    app.cli.add_command(import_data_command)
    app.cli.add_command(import_glottolog_command)
    app.cli.add_command(create_tables_command)
    app.cli.add_command(cache_taxons_command)
    app.cli.add_command(cache_ntonal_command)
    app.cli.add_command(cache_dialects_command)
    app.cli.add_command(cache_maps_command)
