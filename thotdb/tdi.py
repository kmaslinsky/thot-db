import re
from collections import defaultdict


class MarkupError(object):
    def __init__(self, message, token):
        self.message = message
        self.token = token


def _join_markup(tks):
    txt = '.|-#'.join(tks)
    txt = re.sub('[+]([^#]*)[.][|]-#', r'\1-#', txt)
    txt = re.sub('[=]([^#]*)[.][|]-#', r'\1.-#', txt)
    txt = re.sub(r'\[([fxHLM]+) ([.,|-]+)', r'\2[\1 ', txt)
    txt = re.sub(r'([.,|-]+)\]', r']\1', txt)
    txt = re.sub(r'\]([.,|-])>', r']>\1', txt)
    return txt


def markup_string(tokens):
    txt = _join_markup(t.markup for t in tokens)
    return txt


def check_balanced_nonoverlapping(tokens, left, right):
    cnt = 0
    msg = None
    for i, token in enumerate(tokens, start=1):
        for char in token.markup:
            if char == left: cnt += 1
            if char == right: cnt -= 1
            if cnt < 0:
                msg = f'Extra closing {right} in token {i}: {token.markup}'
            elif cnt > 1:
                msg = f'Nested {left} is not allowed in token {i}: {token.markup}'
            if msg:
                return (False, MarkupError(msg, token))
    return (True, None)


def toneme_syntax(tokens, tonemes):
    pattern = r'\[([fxHLM]+)([ \]]|$)'
    for i, token in enumerate(tokens, start=1):
        for m in re.finditer(pattern, token.markup):
            if not m.group(1) in tonemes:
                msg = f'Toneme string in token {i} not in the list of allowed tonemes ({" ".join(tonemes)}): {token.markup}'
                yield (False, MarkupError(msg, token))
    return (True, None)


def markup_errors(tokens, tonemes):
    groups = [
        ('[', ']'), ('<', '>'), ('{', '}')
    ]
    errors = []
    for left, right in groups:
        success, err = check_balanced_nonoverlapping(tokens, left, right)
        if not success:
            errors.append(err)
    for success, err in toneme_syntax(tokens, tonemes):
        if not success:
            errors.append(err)
    return errors


def _count_boundaries(tokens, boundary, exclude=None):
    cnt = 0
    for token in tokens:
        if token.markup:
            for b in re.finditer(boundary, token.markup):
                cnt += 1
            if not exclude:
                cnt += 1
            elif not re.match(exclude, token.markup):
                cnt += 1
    return cnt


def count_syllables(tokens):
    boundary = '([.]|[.]?[|])'
    return _count_boundaries(tokens, boundary, exclude='.*[+]')


def count_feet(tokens):
    boundary = '(?<!_)[|]'
    return _count_boundaries(tokens, boundary, exclude='.*([+=]|[|]_)')


def count_moras(tokens):
    boundary = '([,.]|[.]?[|])'
    return _count_boundaries(tokens, boundary, exclude='.*[+]')


def count_morphemes(tokens):
    boundary = '-'
    return _count_boundaries(tokens, boundary)


def count_tonal_spans(tokens):
    cnt = 0
    for token in tokens:
        for char in token.markup:
            if char == '[':
                cnt += 1
    return cnt
    

def iter_tonal_spans(txt):
    for ts in re.findall(r'([.,|<>#-]*)\[([fxHLM]+) ([^]]+)\]', txt):
        yield ts


def _count_span_units(txt, boundary):
    d = defaultdict(list)
    start = True
    grammatical = False
    for bmarkers, toneme, span in iter_tonal_spans(txt):
        bs = len(re.findall(boundary, bmarkers))
        slength = bs + len(re.findall(boundary, span))
        if start:
            slength += 1
            start = False
        d[toneme].append(slength)
        if '<' in bmarkers:
            grammatical = True
        elif '>' in bmarkers:
            grammatical = False
        if grammatical:
            d[f'g{toneme}'].append(slength)
    return d


def syl_span_length(tokens):
    txt = markup_string(tokens)
    d = _count_span_units(txt, boundary='[.][|]|[.]|[|]')
    return d


def mora_span_length(tokens):
    txt = markup_string(tokens)
    d = _count_span_units(txt, boundary='[.][|]|[.,]|[|]')
    return d
