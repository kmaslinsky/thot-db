import re
from itertools import groupby
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms import validators
from sqlalchemy import or_, func
from thotdb.models import LangInfo, ThotLangInfo


class SearchForm(FlaskForm):
    query = StringField(label='Search')
    submit = SubmitField('Search')


def query_type(query):
    if re.match('[a-z]{3}$', query):
        return 'iso'
    elif re.match('[a-z]{4}[0-9]{4}$', query):
        return 'glottocode'
    else:
        return 'name'


def resolve_dialect(lang):
    if lang.level == 'language':
        return lang
    elif lang.level == 'dialect':
        print('DIALECT:', lang, lang.is_dialect_of)
        return lang.is_dialect_of
    else:
        print('ERROR: Cannot resolve dialect', lang)
    
    
def search_languages(query):
    query = query.strip()
    qt = query_type(query)
    if qt == 'iso':
        langs = LangInfo.query.filter_by(iso=query).all()
    elif qt == 'glottocode':
        langs = LangInfo.query.filter_by(lang_id=query).all()
    else:
        query = query.capitalize()
        langs = LangInfo.query.outerjoin(ThotLangInfo).filter(
            or_(
                LangInfo.name.ilike(query),
                ThotLangInfo.alt_names.ilike(query)
            )
        ).all()
        if not langs:
            # fuzzy match
            langs = LangInfo.query.outerjoin(ThotLangInfo).filter(
                or_(
                    LangInfo.name.regexp_match(fr'.*\b{query}\b', 'I'),
                    ThotLangInfo.alt_names.regexp_match(fr'.*\b{query}\b', 'I')
                )
            ).all()
    result = list(set(map(resolve_dialect, langs)))
    return result




def get_taxon(lang):
    try:
        return lang.taxon
    except AttributeError:
        return None


def group_by_taxons(langs):
    result = {}
    
    def keyfunc(l):
        t = get_taxon(l)
        if t:
            return t.name
        return t
    
    langs = sorted(langs, key=keyfunc)
    for taxon, langs in groupby(langs, get_taxon):
        result[taxon] = list(langs)
    return result
