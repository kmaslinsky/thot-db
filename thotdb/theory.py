from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, session
)
from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, IntegerField, SubmitField
from wtforms.widgets import RadioInput
from wtforms import validators
from wtforms_sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from thotdb.widgets import MultiCheckboxField, InfoStatusField, RadioListWidget, info_status
from werkzeug.exceptions import abort
from thotdb.auth import login_required
from thotdb.models import db
from thotdb.models import TonemeCriteria, CriterionType


bp = Blueprint('theory', __name__)


class CriterionForm(FlaskForm):
    cname = StringField(label="Short name", validators=[validators.input_required()])
    ctype = QuerySelectField(
        label="Criterion type",
        query_factory=lambda: CriterionType.query.all(),
        validators=[validators.input_required()],
        allow_blank=False, blank_text='-- select criterion type --'
    )
    description = TextAreaField(label="Description")
    submit = SubmitField('Save changes')


@bp.route('/criteria')
def toneme_criteria():
    session['url'] = url_for('theory.toneme_criteria')
    criteria = TonemeCriteria.query.all()
    return render_template('theory/criteria.html', criteria=criteria)


@bp.route('/criterion/create')
@login_required
def create_criterion():
    criterion = TonemeCriteria()
    db.session.add(criterion)
    db.session.commit()
    return redirect(url_for('theory.edit_criterion', id=criterion.id))


@bp.route('/criterion/<int:id>/edit', methods=('GET', 'POST'))
def edit_criterion(id):
    criterion = TonemeCriteria.query.get_or_404(id)
    session['url'] = url_for('theory.edit_criterion', id=id)
    form = CriterionForm()
    if g.user is None:
        for f in form:
            setattr(f, 'render_kw', {'disabled':''})

    if g.user is not None:
        if form.validate_on_submit():
            criterion.name = form.cname.data
            criterion.description = form.description.data
            criterion.type = form.ctype.data.id
            db.session.add(criterion)
            db.session.commit()
            return redirect(url_for('theory.toneme_criteria'))
        elif request.method == 'POST':
            flash(f"Invalid input value: {form.errors}")

    form.cname.data = criterion.name
    form.ctype.data = criterion.ctype
    form.description.data = criterion.description
    return render_template('theory/edit_criterion.html', criterion=criterion, form=form)


@bp.route('/criterion/<int:id>/delete', methods=('POST',))
@login_required
def delete_criterion(id):
    criterion = TonemeCriteria.query.get(id)
    db.session.delete(criterion)
    db.session.commit()
    return {
        "criterion_id": id
    }
