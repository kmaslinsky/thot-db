import folium
from flask import (Blueprint, flash, g, redirect, render_template,
                   request, url_for, session)
from thotdb.models import MapsCache, TonalStatus, LangInfo, Taxons
from thotdb.models import db
from thotdb.search import SearchForm, search_languages


bp = Blueprint('maps', __name__)


def get_map_from_cache(page, selector=''):
    cacheobj = MapsCache.query.filter_by(page=page, selector=selector).one_or_none()
    if cacheobj and not cacheobj.invalid:
        return cacheobj.mapframe
    return None


def cache_map(page, selector, mapframe):
    m = MapsCache.query.filter_by(page=page, selector=selector).one_or_none()
    if not m:
        m = MapsCache(page=page, selector=selector)
    m.mapframe = mapframe
    m.invalid = False
    db.session.add(m)
    db.session.commit()


def invalidate_cache(page, selector=None):
    if selector:
        ms = MapsCache.query.filter_by(page=page, selector=selector).all()
    else:
        ms = MapsCache.query.filter_by(page=page).all()
    for m in ms:
        m.invalid = True


def lang_color(lang_info, icon=False):
    category = lang_info.tonal_status
    dc = {
        'tonal': {
            'fill_color': '#009e73',
            'icon': 'green',
            'link_class': 'link-success'
        },
        'toneless': {
            'fill_color': '#f0e442',
            'icon': 'beige',
            'link_class': 'link-warning'
        },
        'unknown': {
            'fill_color': '#56b4e9',
            'icon': 'cadetblue',
            'link_class': 'link-primary'
        },
        'checked': 'black'
    }
    clrs = dc[category]
    if icon:
        color = clrs['icon']
    elif lang_info.checked:
        color = dc['checked']
    else:
        color = clrs['fill_color']        
    return (color, clrs['fill_color'], clrs['link_class'])


def lang_popup(lang_info, link_class):
    base = f'''<a target="_parent" class="{link_class}" href="{url_for('reports.edit_tonal_status', lang_id=lang_info.lang_id)}">{lang_info.name}</a> ({lang_info.lang_id}) — {lang_info.tonal_status}'''
    if lang_info.description:
        desc = f'''<a href="{url_for('language.update', lang_id=lang_info.lang_id)}">Tonal system description</a>'''
        if lang_info.description.tdi:
            desc = f'TDI: {round(lang_info.description.tdi, 2)} {desc}'
        return f'{base}<br/>{desc}'
    return base


def lang_tooltip(lang_info):
    return f"{lang_info.name} ({lang_info.lang_id})"
    
    
def lang_marker(lang_info, circle=True, **kwargs):
    color, fill_color, link_class = lang_color(lang_info, icon=not circle)
    popupstr = lang_popup(lang_info, link_class)
    tooltipstr = lang_tooltip(lang_info)
    if circle:
        lm = folium.CircleMarker(location=lang_info.location, radius=5,
                                 color=color, fill_color=fill_color,
                                 popup=popupstr, tooltip=tooltipstr,
                                 weight=1, opacity=1, **kwargs)
    else:
        lm = folium.Marker(lang_info.location,
                           icon=folium.Icon(color), popup=popupstr,
                           tooltip=tooltipstr)        
    return lm


def map_world(selector='tonal', **kwargs):
    cached = get_map_from_cache('map_world', selector)
    if cached:
        return cached
    categories = selector.split('|')
    m = folium.Map(tiles="cartodb positron", **kwargs)
    all_langs = LangInfo.query.filter(LangInfo.level == 'language').all()
    fgs = {
        'unknown': folium.FeatureGroup("languages with unknown tonal status"),
        'tonal': folium.FeatureGroup("tonal languages"),
        'toneless': folium.FeatureGroup("toneless languages"),
    }
    for lang in all_langs:
        cat = lang.tonal_status
        if cat in categories:
            if lang.location is None:
                print('NO COORDINATES FOR: ', lang)
                continue
            lang_marker(lang, cat, fill_opacity=0.5).add_to(fgs[cat])
    for category in categories:
        fgs[category].add_to(m)
    folium.FitOverlays().add_to(m)
    if len(categories) > 1:
        folium.LayerControl().add_to(m)
    iframe = m.get_root()._repr_html_()
    cache_map('map_world', selector, iframe)
    return iframe


def map_family(selector, **kwargs):
    cached = get_map_from_cache('map_world', selector)
    if cached:
        return cached
    m = folium.Map(tiles="cartodb positron", **kwargs)
    m._name = "map_family"
    m._id = selector
    family = Taxons.query.get(selector)
    fg = folium.FeatureGroup(name="famlangs").add_to(m)
    fg._name = "flangs"
    fg._id = "fg"
    for lang in family.languages:
        if lang.location:
            lang_marker(lang, lang.tonal_status, fill_opacity=0.5).add_to(fg)
        else:
            print('NO COORDINATES FOR: ', lang)
    folium.FitOverlays().add_to(m)
    iframe = m.get_root()._repr_html_()
    cache_map('map_family', selector, iframe)
    return iframe


def map_language(lang_info, **kwargs):
    m = folium.Map(lang_info.location, zoom_start=5, tiles="cartodb positron")
    if lang_info.location:
        lang_marker(lang_info, circle=True).add_to(m)
    mapframe = m.get_root()._repr_html_()
    return mapframe


def map_search_results(results, **kwargs):
    m = folium.Map(results[0].location, zoom_start=5, tiles="cartodb positron")
    for lang_info in results:
        if lang_info.location:
            lang_marker(lang_info, circle=True, fill_opacity=1).add_to(m)
    if len(results) > 1:
        folium.FitOverlays().add_to(m)
    mapframe = m.get_root()._repr_html_()
    return mapframe


@bp.route('/mapview', methods=('GET', 'POST'))
def world():
    form = SearchForm()
    mapframe = map_world('unknown|toneless|tonal', max_zoom=10)
    if form.validate_on_submit():
        query = form.query.data
        if query:
            results = search_languages(query)
            if results:
                mapframe = map_search_results(results)
                return render_template('maps/world.html', form=form,
                                       mapframe=mapframe)
            else:
                flash('Nothing found!', 'info')
    elif request.method == 'POST':
        flash(f"Invalid input value: {form.errors}", 'warning')

    return render_template('maps/world.html', mapframe=mapframe, form=form)


@bp.route('/<lang_id>/map', methods=('GET',))
def language(lang_id):
    lang_info = LangInfo.query.get_or_404(lang_id)
    mapframe = map_language(lang_info)
    if not lang_info.location:
        flash(f'Geographical position for {lang_info.name} ({lang_info.lang_id}) is not defined in Glottolog', 'warning')
    return render_template('maps/language.html', lang_info=lang_info,
                           mapframe=mapframe)

