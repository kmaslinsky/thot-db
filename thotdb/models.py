import datetime
import humanize
from typing import Optional
from typing import List
from statistics import mean
from sqlalchemy.orm import Mapped, mapped_column
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func, or_
from sqlalchemy import ForeignKey
from sqlalchemy import Column
from sqlalchemy import Table
from sqlalchemy import MetaData
from sqlalchemy.orm import aliased
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import registry
from flask_sqlalchemy import SQLAlchemy
from thotdb.tdi import count_tonal_spans, count_syllables, count_moras, count_feet, count_morphemes, syl_span_length, mora_span_length, markup_errors
from statistics import mean, median
from thotdb.stats import ntonal, ncandidates, estimate_ntonal, TaxonStats, GlobalStats


class_registry = {}
reg = registry(class_registry=class_registry)


convention = {
    "ix": 'ix_%(column_0_label)s',
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s"
}


class Base(DeclarativeBase):
    # metadata = MetaData(naming_convention=convention)
    registry = reg


db = SQLAlchemy(model_class=Base)


class User(db.Model):
    id: Mapped[int] = mapped_column(primary_key=True)
    fullname: Mapped[str]
    email: Mapped[str] = mapped_column(unique=True)
    username: Mapped[str] = mapped_column(unique=True)
    password: Mapped[str]
    # relationships
    descriptions: Mapped[List["Descriptions"]] = relationship(back_populates="owner", foreign_keys="Descriptions.owner_id")

    def __str__(self):
        return self.fullname


dialects = Table(
    "dialects",
    Base.metadata,
    Column("lang_id", ForeignKey("lang_info.lang_id"), primary_key=True),
    Column("dialect_id", ForeignKey("lang_info.lang_id"), primary_key=True)
)


taxon_langs = Table(
    "taxon_langs",
    Base.metadata,
    Column("taxon_id", ForeignKey("taxons.taxon_id"), primary_key=True),
    Column("lang_id", ForeignKey("lang_info.lang_id"), primary_key=True),
)


class LangInfo(db.Model):
    lang_id: Mapped[str] = mapped_column(primary_key=True)
    name: Mapped[str]
    iso: Mapped[Optional[str]]
    level: Mapped[str]
    family_id: Mapped[Optional[str]] = mapped_column(ForeignKey("taxons.taxon_id"))
    parent_id: Mapped[Optional[str]] = mapped_column(ForeignKey("taxons.taxon_id"))
    latitude: Mapped[Optional[float]]
    longitude: Mapped[Optional[float]]
    macroregion: Mapped[str]
    country_ids: Mapped[Optional[str]]
    # relationships
    thot: Mapped["ThotLangInfo"] = relationship(back_populates="lang_info")
    family: Mapped["Taxons"] = relationship(foreign_keys=[family_id])
    parent: Mapped["Taxons"] = relationship(foreign_keys=[parent_id])
    description: Mapped["Descriptions"] = relationship(back_populates="lang_info")
    tonal: Mapped["TonalStatus"] = relationship(back_populates="lang_info")
    lang_parent: Mapped["LangInfo"] = relationship(remote_side=[lang_id],
                                                   primaryjoin='LangInfo.lang_id == foreign(LangInfo.parent_id)',
                                                   back_populates='children', viewonly=True)
    children: Mapped[List["LangInfo"]] = relationship(back_populates='lang_parent',
                                                      primaryjoin='LangInfo.lang_id == foreign(LangInfo.parent_id)', viewonly=True)
    taxon: Mapped["Taxons"] = relationship(secondary='taxon_langs', back_populates='languages')
    is_dialect_of: Mapped["LangInfo"] = relationship(secondary='dialects', viewonly=True,
                                                     primaryjoin='LangInfo.lang_id == dialects.c.dialect_id',
                                                     secondaryjoin='LangInfo.lang_id == dialects.c.lang_id'
                                                     )
    dialects: Mapped[List["LangInfo"]] = relationship(secondary='dialects', viewonly=True,
                                                     primaryjoin='LangInfo.lang_id == dialects.c.lang_id',
                                                     secondaryjoin='LangInfo.lang_id == dialects.c.dialect_id'
                                                     )

    def __str__(self):
        return f'{self.name} ({self.lang_id})'

    @property
    def is_tonal(self):
        if self.tonal:
            if self.tonal.status and self.tonal.status.name == 'yes':
                return True
        return False
    
    @property
    def is_nontonal(self):
        if self.tonal:
            if self.tonal.status and self.tonal.status.name == 'no':
                return True
        return False

    @property
    def is_candidate(self):
        if self.is_tonal:
            if self.tonal.data_status_id and self.tonal.data_status_id > 2:
                return False
            return True
        return False

    @property
    def tonal_status(self):
        if self.is_tonal:
            return 'tonal'
        elif self.is_nontonal:
            return 'toneless'
        else:
            return 'unknown'

    @property
    def checked(self):
        def boolify(value):
            if value is None:
                return value
            return value not in ('N', 'None')
        if self.tonal:
            if self.tonal.checked:
                return True
            else:
                sources = list(filter(lambda s: s is not None,
                                      map(boolify,
                                          (self.tonal.hyman, self.tonal.lapsyd, self.tonal.wals))))
                if not sources:
                    return False
                return all(sources) or not any(sources)
        
    @property
    def location(self):
        if not self.latitude and not self.longitude:
            return None
        return (self.latitude, self.longitude)


class ThotLangInfo(db.Model):
    lang_id: Mapped[str] = mapped_column(ForeignKey("lang_info.lang_id"), primary_key=True)
    alt_names: Mapped[Optional[str]]
    microregion: Mapped[Optional[str]]
    parent_id: Mapped[Optional[str]] = mapped_column(ForeignKey("taxons.taxon_id"))
    l1speakers: Mapped[Optional[int]]
    l2speakers: Mapped[Optional[int]]
    dialect: Mapped[Optional[str]]
    # relationships
    lang_info: Mapped["LangInfo"] = relationship(back_populates="thot")
    parent: Mapped["Taxons"] = relationship()


leaf_taxons = Table(
    "leaf_taxons",
    Base.metadata,
    Column("taxon_id", ForeignKey("taxons.taxon_id"), primary_key=True),
    Column("lang_id", ForeignKey("lang_info.lang_id"), primary_key=True),
)


class Taxons(db.Model):
    taxon_id: Mapped[str] = mapped_column(primary_key=True)
    name: Mapped[Optional[str]]
    family_id: Mapped[Optional[str]] = mapped_column(ForeignKey("taxons.taxon_id"))
    parent_id: Mapped[Optional[str]] = mapped_column(ForeignKey("taxons.taxon_id"))
    child_family_count: Mapped[int]
    child_language_count: Mapped[int]
    # tonal_count: Mapped[int] = mapped_column(default=0)
    # toneless_count: Mapped[int] = mapped_column(default=0)
    # unknown_count: Mapped[int] = mapped_column(default=0)
    # relationships
    quota: Mapped["Quotas"] = relationship(back_populates="taxon")
    family: Mapped["Taxons"] = relationship(remote_side=[taxon_id], foreign_keys=[family_id])
    parent: Mapped["Taxons"] = relationship(remote_side=[taxon_id], foreign_keys=[parent_id])
    children: Mapped[List["Taxons"]] = relationship(back_populates="parent", foreign_keys=[parent_id])
    languages: Mapped[List["LangInfo"]] = relationship(secondary=taxon_langs, back_populates="taxon")
    langshere: Mapped[List["LangInfo"]] = relationship(secondary=leaf_taxons)

    @property
    def statshere(self):
        stats = TaxonStats()
        for lang in self.langshere:
            if lang.is_tonal:
                stats.tonal += 1
            elif lang.is_nontonal:
                stats.toneless += 1
            else:
                stats.unknown += 1
        for child in self.children:
            stats += child.statshere
        return stats

    def get_parents(self):
        ps = [self]
        if self.family == self.parent:
            ps.append(self.parent)
            return ps
        else:
            ps.extend(self.parent.get_parents())
            return ps

    @property
    def parents(self):
        ps = list(filter(None, self.get_parents()))
        return ' > '.join(str(p) for p in ps[::-1])
    
    def __str__(self):
        return f"{self.name} ({self.taxon_id})"


class Descriptions(db.Model):
    lang_id: Mapped[str] = mapped_column(ForeignKey("lang_info.lang_id"), primary_key=True)
    author: Mapped[Optional[str]]
    reference: Mapped[Optional[str]]
    owner_id: Mapped[Optional[int]] = mapped_column(ForeignKey("user.id"))
    operator_id: Mapped[int] = mapped_column(ForeignKey("user.id"))
    modified: Mapped[datetime.datetime] = mapped_column(server_default=func.now())
    status_id: Mapped[int] = mapped_column(ForeignKey("desc_status.id"))
    report_doi: Mapped[Optional[str]]
    # relationships
    owner: Mapped["User"] = relationship(back_populates="descriptions", foreign_keys=[owner_id])
    operator: Mapped["User"] = relationship(foreign_keys=[operator_id])
    status: Mapped["DescStatus"] = relationship(back_populates="desc")
    lang_info: Mapped["LangInfo"] = relationship(back_populates="description")
    texts: Mapped[List["TextMetadata"]] = relationship(primaryjoin="Descriptions.lang_id == foreign(TextMetadata.lang_id)")
    dialect: Mapped["SourceDialect"] = relationship(back_populates="desc", cascade="all, delete, delete-orphan")
    tonal_system: Mapped["TonalSystem"] = relationship(primaryjoin="Descriptions.lang_id == foreign(TonalSystem.lang_id)")

    @property
    def tdi(self):
        if self.texts:
            tdi_list = list(filter(None, [t.tdi for t in self.texts]))
            if tdi_list:
                return mean(tdi_list)
        return None

    @property
    def age(self):
        if self.modified.date() == datetime.datetime.today():
            return humanize.naturaltime(datetime.datetime.now() - self.modified)
        else:
            return f'{humanize.naturaldelta(datetime.datetime.today().date() - self.modified.date())} ago'


class DescStatus(db.Model):
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]
    color: Mapped[Optional[str]]
    # relationships
    desc: Mapped["Descriptions"] = relationship(back_populates="status")

    def __str__(self):
        return self.name


class Quotas(db.Model):
    taxon_id: Mapped[str] = mapped_column(ForeignKey("taxons.taxon_id"), primary_key=True)
    tonal: Mapped[Optional[int]]
    nontonal: Mapped[Optional[int]]
    totaltonal: Mapped[Optional[int]]
    quota: Mapped[int]
    candidates: Mapped[Optional[int]]
    # relationships
    taxon: Mapped["Taxons"] = relationship(back_populates="quota")

    @property
    def descriptions(self):
        lang_ids = [l.lang_id for l in self.taxon.languages]
        return Descriptions.query.filter(Descriptions.lang_id.in_(lang_ids)).all()

    @classmethod
    def quotedlangs(self):
        langs = []
        for q in Quotas.query.all():
            langs.extend([l.lang_id for l in q.taxon.languages])
        return langs

    @classmethod
    def update_all(self):
        for q in Quotas.query.all():
            q.update_stats()
    
    def update_stats(self):
        taxon = self.taxon
        tonal, nontonal = ntonal(taxon)
        self.tonal = tonal
        self.nontonal = nontonal
        self.candidates = ncandidates(taxon)
        self.totaltonal = estimate_ntonal(taxon)
        db.session.add(taxon)
        db.session.commit()


class TonalStatus(db.Model):
    lang_id: Mapped[str] = mapped_column(ForeignKey("lang_info.lang_id"), primary_key=True)
    tonal: Mapped[Optional[int]] = mapped_column(ForeignKey("info_status.id"))
    checked: Mapped[Optional[str]]
    hyman: Mapped[Optional[int]]
    lapsyd: Mapped[Optional[str]]
    wals: Mapped[Optional[str]]
    hammarstrom: Mapped[Optional[int]] = mapped_column(ForeignKey("info_status.id"))
    thot: Mapped[Optional[bool]]
    comment: Mapped[Optional[str]]
    data_status_id: Mapped[Optional[int]] = mapped_column(ForeignKey("data_status.id"))
    data_comment: Mapped[Optional[str]]
    # relationship
    lang_info: Mapped["LangInfo"] = relationship(back_populates="tonal")
    status: Mapped["InfoStatus"] = relationship(foreign_keys="TonalStatus.tonal")
    auto: Mapped["InfoStatus"] = relationship(foreign_keys="TonalStatus.hammarstrom")
    data_status: Mapped["DataStatus"] = relationship()


class TonalStatusStats(db.Model):
    id: Mapped[int] = mapped_column(primary_key=True)
    date: Mapped[datetime.date] = mapped_column(unique=True)
    tonal: Mapped[int]
    toneless: Mapped[int]
    total: Mapped[int]
    estimate: Mapped[int]
    checked: Mapped[int]
    quota: Mapped[int]

    @property
    def unknown(self):
        return self.total - self.tonal - self.toneless

    @classmethod
    def weekago(self):
        weekago = datetime.date.today() - datetime.timedelta(days=7)
        prev = TonalStatusStats.query.filter(TonalStatusStats.date <= weekago).order_by(TonalStatusStats.date.desc()).first()
        return prev

    @classmethod
    def current_stats(self):
        tonal = db.session.query(func.sum(Quotas.tonal)).scalar()
        toneless = db.session.query(func.sum(Quotas.nontonal)).scalar()
        estimate = db.session.query(func.sum(Quotas.totaltonal)).scalar()
        quota = db.session.query(func.sum(Quotas.quota)).scalar()
        total = 0
        for q in Quotas.query.all():
            total += len(q.taxon.languages)
        unknown = total - tonal - toneless
        checked = 0
        for ts in TonalStatus.query.all():
            if ts.lang_info.checked:
                checked += 1
        return GlobalStats(total=total, tonal=tonal,
                           toneless=toneless, unknown=unknown,
                           checked=checked, estimate=estimate,
                           quota=quota)

    @classmethod
    def save_stats(self):
        today = datetime.date.today()
        today_stats = self.query.filter_by(date=today).one_or_none()
        if not today_stats:
            today_stats = self(date=today)
        stats = self.current_stats()
        print('CURRENT STATS', stats)
        for key, value in vars(stats).items():
            if not key == 'unknown':
                setattr(today_stats, key, value)
        db.session.add(today_stats)
        db.session.commit()


    
    def __str__(self):
        return f'{self.date.isoformat()}: total {self.total}, tonal {self.tonal}, toneless {self.toneless}, unknown {self.unknown}, checked {self.checked}, estimate {self.estimate}, quota {self.quota}'


class InfoStatus(db.Model):
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]

    def __str__(self) -> str:
        return self.name


class TonalSystem(db.Model):
    lang_id: Mapped[str] = mapped_column(ForeignKey("lang_info.lang_id"), primary_key=True)
    tbu: Mapped[Optional[int]] = mapped_column(ForeignKey("tbu_units.tbu_id"))
    prosodic_foot: Mapped[Optional[int]] = mapped_column(ForeignKey("info_status.id"))
    prosodic_foot_relevance: Mapped[Optional[int]] = mapped_column(ForeignKey("info_status.id"))
    tonal_domain_size_min: Mapped[Optional[int]] = mapped_column(ForeignKey("td_minsize.id"))
    tonal_domain_size_max: Mapped[Optional[int]] = mapped_column(ForeignKey("td_maxsize.id"))
    td_proto_unit: Mapped[Optional[int]] = mapped_column(ForeignKey("boundaries.id"))
    td_gt_proto: Mapped[Optional[str]]
    td_lt_proto: Mapped[Optional[str]]
    tonal_levels: Mapped[Optional[int]] = mapped_column(ForeignKey("tonal_levels.id"))
    # relationships
    td_boundaries: Mapped[List["Boundaries"]] = relationship(secondary="td_boundaries")
    tdprotounit: Mapped["Boundaries"] = relationship("Boundaries")
    tbu_unit: Mapped["TbuUnits"] = relationship("TbuUnits")
    tdminsize: Mapped["TdMinsize"] = relationship("TdMinsize")
    tdmaxsize: Mapped["TdMaxsize"] = relationship("TdMaxsize")
    numtlevels: Mapped["TonalLevels"] = relationship("TonalLevels")
    tonemic_components: Mapped[List["NonlevelComponents"]] = relationship(secondary="nonlevel_components_list")
    tonemes: Mapped[List["TonemesInventory"]] = relationship(cascade="all, delete, delete-orphan")
    tonal_rules: Mapped[List["TonalRules"]] = relationship(cascade="all, delete, delete-orphan")
    orthography: Mapped["Orthography"] = relationship(cascade="all, delete, delete-orphan")
    grammatical_tones: Mapped["GrammaticalTones"] = relationship(cascade="all, delete, delete-orphan")


class TbuUnits(db.Model):
    tbu_id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]

    def __str__(self) -> str:
        return self.name


class TdMinsize(db.Model):
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]

    def __str__(self) -> str:
        return self.name


class TdMaxsize(db.Model):
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]
    value: Mapped[Optional[int]]

    def __str__(self) -> str:
        return self.name


td_boundaries = Table(
    "td_boundaries",
    db.Model.metadata,
    Column("lang_id", ForeignKey("tonal_system.lang_id"), primary_key=True),
    Column("td_boundary", ForeignKey("boundaries.id"), primary_key=True)
)
    

class Boundaries(db.Model):
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(unique=True)

    def __str__(self) -> str:
        return self.name


tonal_levels_list = Table(
    "tonal_levels_list",
    db.Model.metadata,
    Column("num", ForeignKey("tonal_levels.id"), primary_key=True),
    Column("tlevel", ForeignKey("levels_inventory.id"), primary_key=True))


class LevelsInventory(db.Model):
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]

    def __str__(self) -> str:
        return self.name


class TonalLevels(db.Model):
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[int]
    # relationship
    levels: Mapped[List["LevelsInventory"]] = relationship(secondary="tonal_levels_list")

    def __str__(self) -> str:
        return f"{str(self.name)} ({' '.join(i.name for i in self.levels)})"


nonlevel_components_list = Table(
    "nonlevel_components_list",
    db.Model.metadata,
    Column("lang_id", ForeignKey("tonal_system.lang_id"), primary_key=True),
    Column("component", ForeignKey("nonlevel_components.id"), primary_key=True)
)


class NonlevelComponents(db.Model):
    id: Mapped[int] = mapped_column(ForeignKey("levels_inventory.id"), primary_key=True)
    description: Mapped[str]
    # relationship
    obj: Mapped["LevelsInventory"] = relationship()

    def __str__(self) -> str:
        return f"{self.obj.name} ({self.description})"


class TonemesInventory(db.Model):
    toneme_id: Mapped[int] = mapped_column(primary_key=True)
    lang_id: Mapped[int] = mapped_column(ForeignKey("tonal_system.lang_id"))
    toneme_name: Mapped[Optional[str]]
    default_tone: Mapped[bool] = mapped_column(default=False)
    # relationship
    components: Mapped[List["TonemesComposition"]] = relationship(back_populates="toneme",
                                                                  order_by='TonemesComposition.order',
                                                                  cascade="all, delete, delete-orphan")
    allotones: Mapped[List["Allotones"]] = relationship(cascade="all, delete")
    criteria: Mapped[List["TonemeStatus"]] = relationship(cascade="all, delete, delete-orphan", back_populates="toneme")
    nonpitch: Mapped[List["NonpitchComponents"]] = relationship(cascade="all, delete, delete-orphan")
    lang_info: Mapped["LangInfo"] = relationship(primaryjoin="TonemesInventory.lang_id==LangInfo.lang_id", foreign_keys="TonemesInventory.lang_id", viewonly=True)

    def __str__(self):
        return ''.join([str(c) for c in self.components])


class TonemesComposition(db.Model):
    toneme_id: Mapped[int] = mapped_column(ForeignKey("tonemes_inventory.toneme_id"), primary_key=True)
    order: Mapped[int] = mapped_column(primary_key=True)
    tlevel: Mapped[int] = mapped_column(ForeignKey("levels_inventory.id"))
    floating: Mapped[bool] = mapped_column(default=False)
    # relationship
    toneme: Mapped["TonemesInventory"] = relationship(back_populates="components")
    component: Mapped["LevelsInventory"] = relationship()

    def __str__(self) -> str:
        if self.floating:
            return f'f{str(self.component)}'
        return str(self.component)

    def __eq__(self, other) -> bool:
        return self.order == other.order and self.tlevel == other.tlevel


class Allotones(db.Model):
    allotone_id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[Optional[str]]
    toneme_id: Mapped[int] = mapped_column(ForeignKey("tonemes_inventory.toneme_id"))
    # relationship
    components: Mapped[List["AllotonesComposition"]] = relationship(back_populates="allotone", cascade="all, delete")


class AllotonesComposition(db.Model):
    allotone_id: Mapped[int] = mapped_column(ForeignKey("allotones.allotone_id"), primary_key=True)
    order: Mapped[int] = mapped_column(primary_key=True)
    tlevel: Mapped[int] = mapped_column(ForeignKey("levels_inventory.id"))
    floating: Mapped[bool] = mapped_column(default=False)
    # relationship
    allotone: Mapped["Allotones"] = relationship(back_populates="components")
    component: Mapped["LevelsInventory"] = relationship()

    def __str__(self) -> str:
        return str(self.component)


class CriterionType(db.Model):
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]

    def __str__(self) -> str:
        return self.name


class TonemeCriteria(db.Model):
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[Optional[str]]
    description: Mapped[Optional[str]]
    type: Mapped[Optional[int]] = mapped_column(ForeignKey("criterion_type.id"))
    # relationships
    ctype: Mapped["CriterionType"] = relationship()
    toneme_list: Mapped[List["TonemeStatus"]] = relationship(back_populates='critobj')


class TonemeStatus(db.Model):
    toneme_id: Mapped[int] = mapped_column(ForeignKey("tonemes_inventory.toneme_id"), primary_key=True)
    criterion: Mapped[int] = mapped_column(ForeignKey("toneme_criteria.id"), primary_key=True)
    comment: Mapped[Optional[str]]
    # relationships
    toneme: Mapped["TonemesInventory"] = relationship(back_populates="criteria")
    critobj: Mapped["TonemeCriteria"] = relationship(back_populates='toneme_list')
    


class NonpitchFeatures(db.Model):
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]
    # FIXME: add it back
    # description: Mapped[Optional[str]]


class NonpitchComponents(db.Model):
    toneme_id: Mapped[int] = mapped_column(ForeignKey("tonemes_inventory.toneme_id"), primary_key=True)
    component: Mapped[int] = mapped_column(ForeignKey("nonpitch_features.id"), primary_key=True)
    # FIXME: rename to comment for consistency
    other: Mapped[Optional[str]]
    # relationships
    feature: Mapped["NonpitchFeatures"] = relationship()


class TonalizationMethods(db.Model):
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]

    def __str__(self):
        return self.name


class SurfaceTones(db.Model):
    lang_id: Mapped[int] = mapped_column(ForeignKey("tonal_system.lang_id"), primary_key=True)
    downdrift: Mapped[Optional[int]] = mapped_column(ForeignKey("info_status.id"))
    downstep: Mapped[Optional[int]] = mapped_column(ForeignKey("info_status.id"))
    nontoned_syllables: Mapped[Optional[int]] = mapped_column(ForeignKey("info_status.id"))
    tonalization_method_id: Mapped[Optional[int]] = mapped_column(ForeignKey("tonalization_methods.id"))
    default_tlevel: Mapped[Optional[int]] = mapped_column(ForeignKey("levels_inventory.id"))
    nontonal_morphemes: Mapped[Optional[int]] = mapped_column(ForeignKey("info_status.id"))
    obligatority: Mapped[Optional[int]] = mapped_column(ForeignKey("info_status.id"))
    culminativity: Mapped[Optional[int]] = mapped_column(ForeignKey("info_status.id"))
    stress: Mapped[Optional[int]] = mapped_column(ForeignKey("info_status.id"))
    stress_td_association: Mapped[Optional[int]] = mapped_column(ForeignKey("info_status.id"))
    toneme_syntax_association: Mapped[Optional[int]] = mapped_column(ForeignKey("info_status.id"))
    syntactic_tonal_rules: Mapped[Optional[int]] = mapped_column(ForeignKey("info_status.id"))
    # relationship
    tonalization_method: Mapped["TonalizationMethods"] = relationship()


class PhoneticBoundaries(db.Model):
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]


class TonalRulesBoundaries(db.Model):
    id: Mapped[int] = mapped_column(primary_key=True)
    lang_id: Mapped[int] = mapped_column(ForeignKey("tonal_system.lang_id"))
    rule_boundary: Mapped[int] = mapped_column(ForeignKey("phonetic_boundaries.id"))


class RuleTypes(db.Model):
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]

    def __str__(self):
        return self.name


rule_dependencies = Table(
    "rule_dependencies",
    Base.metadata,
    Column("rule_id", ForeignKey("tonal_rules.rule_id"), primary_key=True),
    Column("depends_on", ForeignKey("tonal_rules.rule_id"), primary_key=True)
)


class TonalRules(db.Model):
    rule_id: Mapped[int] = mapped_column(primary_key=True)
    lang_id: Mapped[str] = mapped_column(ForeignKey("tonal_system.lang_id"))
    type_id: Mapped[Optional[int]] = mapped_column(ForeignKey("rule_types.id"))
    name: Mapped[Optional[str]]
    formula: Mapped[Optional[str]]
    comment: Mapped[Optional[str]]
    # relationship
    rule_type: Mapped["RuleTypes"] = relationship()
    depends_on: Mapped[List["TonalRules"]] = relationship(secondary='rule_dependencies', 
                                                          primaryjoin='TonalRules.rule_id == rule_dependencies.c.rule_id',
                                                          secondaryjoin='TonalRules.rule_id == rule_dependencies.c.depends_on')

    def __str__(self):
        return f'{self.name}'


class TextMetadata(db.Model):
    text_id: Mapped[int] = mapped_column(primary_key=True)
    lang_id: Mapped[str] = mapped_column(ForeignKey("tonal_system.lang_id"))
    filename: Mapped[Optional[str]]
    reference: Mapped[Optional[str]]
    author: Mapped[Optional[str]]
    date: Mapped[Optional[datetime.date]]
    genre: Mapped[Optional[str]]
    url: Mapped[Optional[str]]
    modified: Mapped[datetime.datetime] = mapped_column(server_default=func.now(), onupdate=func.now())
    operator_id: Mapped[int] = mapped_column(ForeignKey("user.id"))
    # relationship
    tokens: Mapped[List["Tokens"]] = relationship(back_populates="meta", cascade="all, delete, delete-orphan")
    tonal_system: Mapped["TonalSystem"] = relationship(viewonly=True)

    @property
    def as_string(self):
        if not self.tokens:
            return ''
        return ' '.join(t.token or '' for t in self.tokens)

    @property
    def words(self):
        if not self.tokens:
            return 0
        ws = [w for w in self.tokens if w.markup]
        return len(ws)
    
    @property
    def morphemes(self):
        if not self.tokens:
            return None
        return count_morphemes(self.tokens)

    @property
    def feet(self):
        if not self.tokens:
            return None
        return count_feet(self.tokens)

    @property
    def syllables(self):
        if not self.tokens:
            return None
        return count_syllables(self.tokens)

    @property
    def moras(self):
        if not self.tokens:
            return None
        return count_moras(self.tokens)

    @property
    def tonal_spans(self):
        if not self.tokens:
            return None
        return count_tonal_spans(self.tokens)

    @property
    def tdi_univ(self):
        """Return a TDI value based on TBU in a language: mora-based for
        moraic languages, and syllable-based for everything else
        (default)"""
        if not self.tokens:
            return None
        if self.tonal_system.tbu_unit.name == "mora":
            return self.tonal_spans / self.moras
        else:
            return self.tonal_spans / self.syllables
    
    @property
    def tdi(self):
        """Return a TDI syllable-based value)"""
        if not self.tokens:
            return None
        return self.tonal_spans / self.syllables

    @property
    def tdi_type(self):
        if self.tonal_system.tbu_unit.name == "mora":
            return "moraic"
        else:
            return "syllabic"

    @property
    def span_length(self):
        if not self.tokens:
            return None
        if self.tonal_system.tbu_unit.name == "mora":
            return mora_span_length(self.tokens)
        else:
            return syl_span_length(self.tokens)

    def toneme_spans(self, toneme):
        if not self.tokens:
            return None
        return len(self.span_length[toneme])

    def toneme_count(self, grammatical=False):
        if not self.tokens:
            return None
        d = {}
        for toneme in self.tonal_system.tonemes:
            tkey = str(toneme)
            if grammatical:
                ntonemes = len(self.span_length[f'g{tkey}'])
            else:
                ntonemes = len(self.span_length[tkey])
            d[tkey] = ntonemes
        return d

    @property
    def ngrammatical(self):
        tot = 0
        for t, n in self.toneme_count(grammatical=True).items():
            tot += n
        return tot
    
    @property
    def toneme_mean_span(self):
        if not self.tokens:
            return None
        d = {}
        for toneme in self.tonal_system.tonemes:
            spans = self.span_length[str(toneme)] + self.span_length[f'g{str(toneme)}'] or [0]
            ntonemes = mean(spans)
            d[str(toneme)] = ntonemes
        return d

    @property
    def grammatical_toneme_count(self):
        if not self.tokens:
            return None
        d = {}
        
    
    @property
    def errors(self):
        if self.tokens:
            tonal_system = TonalSystem.query.get(self.lang_id)
            tonemes = [str(t) for t in tonal_system.tonemes]
            return markup_errors(self.tokens, tonemes)
        else:
            return None


class Tokens(db.Model):
    token_id: Mapped[int] = mapped_column(primary_key=True)
    text_id: Mapped[int] = mapped_column(ForeignKey("text_metadata.text_id"))
    lang_id: Mapped[str] = mapped_column(ForeignKey("tonal_system.lang_id"))
    token: Mapped[Optional[str]]
    markup: Mapped[Optional[str]]
    gloss: Mapped[Optional[str]]
    # relationship
    meta: Mapped["TextMetadata"] = relationship(back_populates="tokens")

    def __str__(self):
        return f'Token <{self.token_id}> {self.token} {self.markup}'


class Orthography(db.Model):
    lang_id: Mapped[str] = mapped_column(ForeignKey("tonal_system.lang_id"), primary_key=True)
    status_id: Mapped[Optional[int]] = mapped_column(ForeignKey("orthographic_status.id"))
    # relationship
    tones_marked: Mapped["OrthographicStatus"] = relationship()


class OrthographicStatus(db.Model):
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(unique=True)

    def __str__(self) -> str:
        return self.name


class SourceDialect(db.Model):
    id: Mapped[int] = mapped_column(primary_key=True)
    lang_id: Mapped[str] = mapped_column(ForeignKey("descriptions.lang_id"), unique=True)
    dialect_id: Mapped[str] = mapped_column(ForeignKey("lang_info.lang_id"))
    # relationship
    lang_info: Mapped["LangInfo"] = relationship()
    desc: Mapped["Descriptions"] = relationship(back_populates="dialect")


class GrammaticalTones(db.Model):
    lang_id: Mapped[str] = mapped_column(ForeignKey("tonal_system.lang_id"), primary_key=True)
    has_tonal_morphemes: Mapped[Optional[int]] = mapped_column(ForeignKey("info_status.id"))
    tonal_plus_segment: Mapped[Optional[int]] = mapped_column(ForeignKey("info_status.id"))
    has_floating_tones: Mapped[Optional[int]] = mapped_column(ForeignKey("info_status.id"))
    multitoneme_morphemes: Mapped[Optional[int]] = mapped_column(ForeignKey("info_status.id"))
    numtones: Mapped[Optional[int]]
    # relationship
    tonal_functions: Mapped[List["Functions"]] = relationship(secondary="tonal_functions")
    tone_bearers: Mapped[List["ToneBearers"]] = relationship(secondary="grammatical_tone_bearers")


tonal_functions = Table(
    "tonal_functions",
    db.Model.metadata,
    Column("lang_id", ForeignKey("grammatical_tones.lang_id"), primary_key=True),
    Column("function", ForeignKey("functions.id"), primary_key=True)
)


grammatical_tone_bearers = Table(
    "grammatical_tone_bearers",
    db.Model.metadata,
    Column("lang_id", ForeignKey("grammatical_tones.lang_id"), primary_key=True),
    Column("bearer", ForeignKey("tone_bearers.id"), primary_key=True)
)


class ToneBearers(db.Model):
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(unique=True)

    def __str__(self) -> str:
        return self.name
    

class Functions(db.Model):
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(unique=True)

    def __str__(self) -> str:
        return self.name


class DataStatus(db.Model):
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(unique=True)

    def __str__(self) -> str:
        return self.name


class MapsCache(db.Model):
    id: Mapped[int] = mapped_column(primary_key=True)
    page: Mapped[str]
    selector: Mapped[Optional[str]]
    mapframe: Mapped[Optional[str]]
    invalid: Mapped[bool] = mapped_column(default=False)
