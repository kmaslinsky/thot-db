from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, session
)
from sqlalchemy import select
from sqlalchemy.sql import func
from flask import current_app
from flask_wtf import FlaskForm
from wtforms import IntegerField, SubmitField, BooleanField, StringField, TextAreaField, SelectField
from wtforms import validators
from wtforms_sqlalchemy.fields import QuerySelectField
from thotdb.auth import login_required
from thotdb.models import db
from thotdb.models import Quotas, Descriptions, DescStatus, Taxons, LangInfo, TonalStatus, TonalStatusStats, DataStatus
from thotdb.widgets import InfoStatusField
from thotdb.widgets import get_info_status, get_form_info_status
from thotdb.search import SearchForm, search_languages, group_by_taxons
from thotdb.maps import map_world, map_family, invalidate_cache
from thotdb.db import cache_world_maps
from threading import Thread

bp = Blueprint('reports', __name__)


class QuotaForm(FlaskForm):
    quota = IntegerField(label="Current quota:",
                         validators=[validators.input_required()])
    submit = SubmitField('Change quota')


class TonalStatusForm(FlaskForm):
    status = InfoStatusField(label="Language is tonal")
    thot = BooleanField(label="Checked by ThoT")
    checked = StringField(label="Source check done by")
    comment = TextAreaField(label="Information source and comments on the tonal status of the language")
    data_status = QuerySelectField(label="Data for the description of the tonal system",
                                   query_factory=lambda: DataStatus.query.all(),
                                   allow_blank=True
                                   )
    data_comment = TextAreaField(label="Comment on the data source (for ThoT project members only)")
    submit = SubmitField('Save changes')


@bp.route('/dashboard', methods=('GET',))
def dashboard():
    samplesize = 250
    desc = Descriptions.query.order_by(Descriptions.status_id.desc()).all()
    stats = TonalStatusStats.current_stats()
    mapframe = map_world('tonal', max_zoom=10)
    return render_template('reports/dashboard.html',
                           samplesize=samplesize, stats=stats,
                           desc=desc, mapframe=mapframe)
    

@bp.route('/taxons', methods=('GET', 'POST'))
def taxons():
    session['url'] = url_for('reports.taxons')
    form = SearchForm()
    quotas = Quotas.query.join(Quotas.taxon).order_by(Quotas.totaltonal.desc(), Taxons.child_language_count.desc()).all()
    stats = TonalStatusStats.current_stats()
    lastweek = TonalStatusStats.weekago()
    weekdiff = stats - lastweek
    status_colors = {d.name: d.color for d in DescStatus.query.all()}

    if form.validate_on_submit():
        query = form.query.data
        if query:
            langs = search_languages(query)
            results = group_by_taxons(langs)
            return render_template('reports/taxons_serp.html', form=form,
                                   stats=stats, results=results)
    elif request.method == 'POST':
        flash(f"Invalid input value: {form.errors}", 'warning')

    return render_template('reports/taxons.html', form=form,
                           quotas=quotas, stats=stats,
                           weekdiff=weekdiff,
                           status_colors=status_colors)


@bp.route('/progress', methods=('GET',))
def progress():
    session['url'] = url_for('reports.progress')
    quotas = Quotas.query.join(Quotas.taxon).order_by(Quotas.totaltonal.desc(), Taxons.child_language_count.desc()).all()
    global_progress = {
        'total': TonalStatusStats.current_stats().quota,
        'ready': len(Descriptions.query.join(Descriptions.status).filter(DescStatus.name=='ready').all()),
        'editing': len(Descriptions.query.join(Descriptions.status).filter(DescStatus.name=='editing').all()),
        'inprogress': len(Descriptions.query.join(Descriptions.status).filter(DescStatus.name=='in progress').all()),
        'promised': len(Descriptions.query.join(Descriptions.status).filter(DescStatus.name=='promised').all()),
        'sampled': len(Descriptions.query.all())
        }
    status_colors = {d.name: d.color for d in DescStatus.query.all()}
    return render_template('reports/progress.html', quotas=quotas,
                           progress=global_progress, status_colors=status_colors)


@bp.route('/<taxon_id>/quota', methods=('GET', 'POST'))
def edit_quota(taxon_id):
    taxon = Taxons.query.get_or_404(taxon_id)
    quota = Quotas.query.get_or_404(taxon_id)
    form = QuotaForm()

    if g.user is not None:
        if form.validate_on_submit():
            newquota = form.quota.data
            if newquota < len(quota.descriptions):
                flash("Quota cannot be lower than the number of already sampled languages", 'warning')
            else:
                quota.quota = newquota
                db.session.add(quota)
                db.session.commit()
                return redirect(url_for('reports.progress', _anchor=taxon_id))
        elif request.method == 'POST':
            flash(f"Invalid input value: {form.errors}", 'warning')

    form.quota.data = quota.quota
    langs = taxon.languages
    mapframe = map_family(taxon_id, zoom_start=4)
    return render_template('reports/edit_quota.html', taxon=taxon,
                           quota=quota, langs=langs,
                           mapframe=mapframe, form=form)


@bp.route('/<lang_id>/tonal_status', methods=('GET', 'POST'))
def edit_tonal_status(lang_id):
    lang_info = LangInfo.query.get_or_404(lang_id)
    taxon = lang_info.taxon
    if not lang_info.tonal:
        lang_info.tonal = TonalStatus(lang_id=lang_id)
    tonal_status = lang_info.tonal
    status_changed = False
    form = TonalStatusForm(obj=tonal_status)
    if not lang_info.is_tonal:
        setattr(form.data_status, 'render_kw', {'disabled':''})

    if g.user is None:
        for f in form:
            setattr(f, 'render_kw', {'disabled':''})

    if g.user is not None:
        if form.validate_on_submit():
            if not tonal_status.status == form.status:
                status_changed = True
                tonal_status.thot = True
            form.populate_obj(tonal_status)
            if status_changed:
                taxon.quota.update_stats()
                invalidate_cache('map_family', taxon.taxon_id)
                bgjob = Thread(target=cache_world_maps, daemon=True)
                bgjob.start()
            db.session.add(tonal_status)
            db.session.commit()
            TonalStatusStats.save_stats()

        elif request.method == 'POST':
            flash(f"Invalid input value: {form.errors}", 'warning')

    return render_template('reports/tonal_status.html',
                           lang_info=lang_info, taxon=taxon, tonal_status=tonal_status,
                           form=form)
