from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, session
)
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from werkzeug.utils import secure_filename
from wtforms import StringField, TextAreaField, IntegerField, SubmitField, DateField, URLField, SelectField
from wtforms.widgets import RadioInput
from wtforms import validators
from wtforms_sqlalchemy.fields import QuerySelectField
from thotdb.widgets import MultiCheckboxField, InfoStatusField, RadioListWidget, info_status
from werkzeug.exceptions import abort
from thotdb.auth import login_required
from thotdb.models import db
from sqlalchemy import select, or_
import datetime
import re
from openpyxl import load_workbook
from collections import namedtuple
from thotdb.models import LangInfo, ThotLangInfo, Taxons, InfoStatus, TbuUnits, TdMinsize, TdMaxsize, Boundaries, TonalLevels, Descriptions, DescStatus, TonalSystem, User, TonemesInventory, LevelsInventory, TonemesComposition, NonlevelComponents, Allotones, AllotonesComposition, TonemeCriteria, NonpitchFeatures, TonemeStatus, NonpitchComponents, SurfaceTones, TonalizationMethods, TonalRules, RuleTypes, TextMetadata, Tokens, Orthography, OrthographicStatus, SourceDialect, Functions, GrammaticalTones, ToneBearers
from thotdb.tdi import markup_errors
from thotdb.widgets import get_info_status, get_form_info_status
from thotdb.search import SearchForm, query_type


bp = Blueprint('language', __name__)


class SearchForm(FlaskForm):
    query = StringField(label='Simple search', validators=[validators.input_required()])
    submit = SubmitField('Search')


class ThotLangInfoForm(FlaskForm):
    microregion = StringField(label="Microregion",
                              validators=[validators.Optional()])
    alt_names = StringField(label="Alternative names",
                            validators=[validators.Optional()])
    parent = QuerySelectField(
        label="A corrected language family",
        query_factory=lambda: Taxons.query.order_by(Taxons.name).all(),
        allow_blank=True
    )
    l1speakers = IntegerField(label="Number of L1 speakers",
                              validators=[validators.Optional()])
    l2speakers = IntegerField(label="Number of L2 speakers",
                              validators=[validators.Optional()])
    dialect = StringField(label="Source dialect (if not listed in Glottolog)",
                          validators=[validators.Optional()])
    submit = SubmitField('Save changes')


class QuestionnaireForm(FlaskForm):
    owner = QuerySelectField(
        label="Reponsible project member",
        query_factory=lambda: User.query.all(),
        allow_blank=True
    )
    author = StringField(label="Author of the analytical report")
    dialect = SelectField(label="Dialect that served as a primary data source")
    status = QuerySelectField(
        label='Status',
        query_factory=lambda: DescStatus.query.all()
    )
    tbu = QuerySelectField(
        label='TBU',
        query_factory=lambda: TbuUnits.query.all(),
        allow_blank=True)
    prosodic_foot = InfoStatusField(label="Language has a prosodic foot")
    prosodic_foot_relevance = InfoStatusField(
        label="Does prosodic foot play a role in establishing boundaries of tonal spans?")
    tonal_domain_size_min = QuerySelectField(
        label="Minimal tonal span size",
        query_factory=lambda: TdMinsize.query.all(),
        allow_blank=True, blank_value='0')
    tonal_domain_size_max = QuerySelectField(
        label="Maximal tonal span size",
        query_factory=lambda: TdMaxsize.query.all(),
        allow_blank=True, blank_value='0')
    td_boundary = MultiCheckboxField(
        label="Which boundaries can appear inside a tonal span (select all relevant)",
        query_factory=lambda: Boundaries.query.all(),
        allow_blank=True, blank_value='0')
    td_proto_unit = QuerySelectField(
        label="Tonal span prototypically coincides with what unit?",
        query_factory=lambda: Boundaries.query.all(),
        allow_blank=True, blank_value='0')
    td_gt_proto = TextAreaField(label="Describe cases when tonal span may be larger than proto unit")
    td_lt_proto = TextAreaField(label="Describe cases when tonal span may be smaller than proto unit")
    tonal_levels = QuerySelectField(
        label="Number of tonal levels",
        query_factory=lambda: TonalLevels.query.all(),
        widget=RadioListWidget(),
        option_widget=RadioInput(),
        allow_blank=True, blank_value='0')
    tonemic_components = MultiCheckboxField(
        label="Pitch-related phenomena that serve as tonemic components",
        query_factory=lambda: NonlevelComponents.query.all(),
        validators=[validators.optional()])
    default_tlevel = SelectField(
        label="Default tonal level that is assigned to toneless units",
        coerce=int,
        validators=[validators.Optional()])
    downdrift = InfoStatusField(label="Downdrift")
    downstep = InfoStatusField(label="Non-automatic downstep")
    nontoned_syllables = InfoStatusField(label="Superficially toneless syllables (not included in any tonal span)")
    tonalization_method = QuerySelectField(
        label="How toneless syllables are tonalized",
        query_factory=lambda: TonalizationMethods.query.all(),
        allow_blank=True,
        validators=[validators.Optional()])
    nontonal_morphemes = InfoStatusField(label="Toneless morphemes")
    culminativity = InfoStatusField(label="Culminativity")
    obligatority = InfoStatusField(label="Obligatority")
    stress = InfoStatusField(label="Stress")
    stress_td_association = InfoStatusField(label="Is tonal span position predictable given the position of the stress?")
    has_tonal_morphemes = InfoStatusField(label="Does language have grammatical tonal morphemes?")
    has_floating_tones = InfoStatusField(label="Does language have floating grammatical tones?")
    tonal_plus_segment = InfoStatusField(label="Are there tonal morphemes that combine a segment with a tonal operation outside of the segment?")
    multitoneme_morphemes = InfoStatusField(label="Are there tonal morphemes that contain more than one toneme?")
    numtones = IntegerField(label="Estimated number of grammatical tonal morphemes in a language",
                            validators=[validators.Optional()])
    tonal_functions = MultiCheckboxField(
        label="What kinds of grammatical meanings are expressed with the help of tonal morphemes",
        query_factory=lambda: Functions.query.all(),
        allow_blank=True, blank_value='0')
    tone_bearers = MultiCheckboxField(
        label="Grammatical tones are realized on words of the following parts of speech:",
        query_factory=lambda: ToneBearers.query.all(),
        allow_blank=True
    )
    toneme_syntax_association = InfoStatusField(label="Does tonal distribution depend on word classes?")
    orthography = QuerySelectField(label="Are tones marked in the practical orthography of the language?",
                                   query_factory=lambda: OrthographicStatus.query.all(),
                                   allow_blank=True, validators=[validators.Optional()])
    report_doi = StringField(label="Analytical report DOI",
                             validators=[validators.Regexp(r'^(10[.][0-9]{4,}(?:[.][0-9]+)*/(?:(?!["&\'<>])\S)+)$', message='Invalid DOI'),
                                         validators.Optional()])
    save = SubmitField('Save')


class TonalRuleForm(FlaskForm):
    name = StringField(label="Short name", validators=[validators.input_required()])
    rule_type = QuerySelectField(
        label="Rule type",
        query_factory=lambda: RuleTypes.query.all(),
        validators=[validators.input_required()]
    )
    formula = StringField(label="Formula")
    comment = TextAreaField(label="Rule description")
    depends_on = MultiCheckboxField(
        label="This rule is applied after:",
        allow_blank=True
    )
    submit = SubmitField('Save rule')


class TextUploadForm(FlaskForm):
    reference = StringField(label="Text source (bibliographic reference or other attribution)")
    author = StringField(label="Text author or speaker")
    date = DateField(label="Date of recording/publication", validators=[validators.Optional()])
    genre = StringField(label="Text genre")
    url = URLField(label="URL")
    srcfile = FileField(label="Upload file (.xlsx)",
                        validators=[FileAllowed(['xlsx'], 'XLSX only!'),
                                    validators.Optional()])
    submit = SubmitField('Save changes')
    
    
@bp.route('/descriptions')
def index():
    session['url'] = url_for('language.index')
    langs = Descriptions.query.order_by(Descriptions.status_id.desc(), Descriptions.modified.desc()).all()
    return render_template('language/index.html', langs=langs)


@bp.route('/search', methods=('GET', 'POST'))
def search():
    form = SearchForm()
    
    if form.validate_on_submit():
        query = form.query.data
        qt = query_type(query)
        if qt == 'iso':
            langs = LangInfo.query.filter_by(iso=query).all()
        elif qt == 'glottocode':
            langs = LangInfo.query.filter_by(lang_id=query).all()
        else:
            langs = LangInfo.query.outerjoin(ThotLangInfo).filter(
                or_(
                    LangInfo.name.icontains(query),
                    ThotLangInfo.alt_names.icontains(query)
                )
            ).all()
        return render_template('language/search_results.html', results=langs, form=form)
    
    return render_template('language/search.html', form=form)


@bp.route('/<lang_id>/create', methods=(['GET']))
@login_required
def create(lang_id):
    if not Descriptions.query.get(lang_id):
        status = 1 # sampled
        desc = Descriptions(lang_id=lang_id, operator_id=g.user.id, status_id=status)
        db.session.add(desc)
        db.session.commit()
    return redirect(url_for('language.update', lang_id=lang_id))


def update_description(desc, form):
    desc.owner_id = form.owner.data.id if form.owner.data else None
    desc.operator_id = g.user.id
    desc.modified = datetime.datetime.now(datetime.timezone.utc)
    desc.status_id = form.status.data.id
    desc.author = form.author.data
    desc.report_doi = form.report_doi.data
    if form.dialect.data:
        if form.dialect.data == 'thot1234':
            pass
        elif desc.dialect:
            desc.dialect.dialect_id = form.dialect.data
        else:
            sd = SourceDialect(lang_id=desc.lang_id, dialect_id=form.dialect.data)
            db.session.add(sd)
            desc.dialect = sd
    elif desc.dialect: # db has non-null value but the form returned an empty one
        db.session.delete(desc.dialect)
    db.session.add(desc)
    db.session.commit()


def add_toneme_component(toneme: TonemesInventory, component: LevelsInventory) -> TonemesInventory:
    try:
        order = toneme.components[-1].order
    except IndexError:
        order = 0
    tpart = TonemesComposition(toneme_id=toneme.toneme_id, order = order+1, tlevel = component.id)
    toneme.components.append(tpart)
    return toneme


def add_allotone_component(allotone: Allotones, component: LevelsInventory, floating: bool) -> Allotones:
    try:
        order = allotone.components[-1].order
    except IndexError:
        order = 0
    tpart = AllotonesComposition(allotone_id=allotone.allotone_id, order=order+1, tlevel=component.id, floating=floating)
    allotone.components.append(tpart)
    return allotone
    

def update_tonal_system(lang_id, tonal_system, surface_tones, form):
    if tonal_system is None:
        tonal_system = TonalSystem(lang_id=lang_id)
        db.session.add(tonal_system)
    tonal_system.tbu_unit = form.tbu.data
    tonal_system.prosodic_foot = get_form_info_status(form.prosodic_foot.data)
    tonal_system.prosodic_foot_relevance = get_form_info_status(form.prosodic_foot_relevance.data)
    tonal_system.tdminsize = form.tonal_domain_size_min.data
    tonal_system.tdmaxsize = form.tonal_domain_size_max.data
    tonal_system.tdprotounit = form.td_proto_unit.data
    tonal_system.td_gt_proto = form.td_gt_proto.data
    tonal_system.td_lt_proto = form.td_lt_proto.data
    tonal_system.tonal_levels = get_form_info_status(form.tonal_levels.data)
    tonal_system.td_boundaries = form.td_boundary.data
    tonal_system.tonemic_components = form.tonemic_components.data
    if not tonal_system.grammatical_tones:
        tonal_system.grammatical_tones = GrammaticalTones(lang_id=lang_id)
    tonal_system.grammatical_tones.has_tonal_morphemes = get_form_info_status(form.has_tonal_morphemes.data)
    tonal_system.grammatical_tones.tonal_plus_segment = get_form_info_status(form.tonal_plus_segment.data)
    tonal_system.grammatical_tones.has_floating_tones = get_form_info_status(form.has_floating_tones.data)
    tonal_system.grammatical_tones.multitoneme_morphemes = get_form_info_status(form.multitoneme_morphemes.data)
    tonal_system.grammatical_tones.numtones = form.numtones.data
    tonal_system.grammatical_tones.tonal_functions = form.tonal_functions.data
    tonal_system.grammatical_tones.tone_bearers = form.tone_bearers.data
    if not tonal_system.orthography:
        tonal_system.orthography = Orthography(lang_id=lang_id)
    tonal_system.orthography.tones_marked = form.orthography.data
    if surface_tones is None:
        surface_tones = SurfaceTones(lang_id=lang_id)
        db.session.add(surface_tones)
    surface_tones.downdrift = get_form_info_status(form.downdrift.data)
    surface_tones.downstep = get_form_info_status(form.downstep.data)
    surface_tones.nontoned_syllables = get_form_info_status(form.nontoned_syllables.data)
    surface_tones.tonalization_method = form.tonalization_method.data
    if form.default_tlevel.data:
        surface_tones.default_tlevel = form.default_tlevel.data
    elif surface_tones.default_tlevel: # db has non-null value but form returned an empty one
        surface_tones.default_tlevel = None
    surface_tones.nontonal_morphemes = get_form_info_status(form.nontonal_morphemes.data)
    surface_tones.obligatority = get_form_info_status(form.obligatority.data)
    surface_tones.culminativity = get_form_info_status(form.culminativity.data)
    surface_tones.stress = get_form_info_status(form.stress.data)
    surface_tones.stress_td_association = get_form_info_status(form.stress_td_association.data)
    surface_tones.toneme_syntax_association = get_form_info_status(form.toneme_syntax_association.data)
    db.session.commit()


def get_toneme_components(lang_id):
    tonal_system = TonalSystem.query.get(lang_id)
    if tonal_system is None:
        return []
    try:
        tlevels = list(TonalLevels.query.filter_by(id=tonal_system.tonal_levels).scalar().levels)
    except AttributeError:
        tlevels = []
    pcomponents = [c.obj for c in tonal_system.tonemic_components]
    tlevels.extend(pcomponents)
    return tlevels


@bp.route('/<lang_id>/update', methods=('GET', 'POST'))
def update(lang_id):
    desc = Descriptions.query.get_or_404(lang_id)
    lang_info = LangInfo.query.get(lang_id)
    tonal_system = TonalSystem.query.get(lang_id)
    surface_tones = SurfaceTones.query.get(lang_id)
    tonemes = TonemesInventory.query.filter_by(lang_id=lang_id)
    components = get_toneme_components(lang_id)
    criteria = TonemeCriteria.query.all()
    form = QuestionnaireForm()
    if g.user is None:
        for f in form:
            setattr(f, 'render_kw', {'disabled':''})
    
    form.default_tlevel.choices = [(0, '')] + [(c.id, c.name) for c in components]
    if lang_info.thot and lang_info.thot.dialect:
       form.dialect.choices = [('thot1234', lang_info.thot.dialect)]
    else:
        dialects = [('', '')] + [(d.lang_id, str(d)) for d in lang_info.dialects]
        form.dialect.choices = dialects

    if g.user is not None:
        if form.validate_on_submit():
            update_description(desc, form)
            update_tonal_system(lang_id, tonal_system, surface_tones, form)
            return redirect(url_for('language.update', lang_id=lang_id))
        elif request.method == 'POST':
            flash(f"Invalid input value: {form.errors}", 'warning')

    form.owner.data = desc.owner
    form.author.data = desc.author
    form.status.data = desc.status
    form.report_doi.data = desc.report_doi
    if lang_info.thot and lang_info.thot.dialect:
        form.dialect.data = 'thot1234'
    elif desc.dialect:
        form.dialect.data = desc.dialect.lang_info.lang_id
    if tonal_system is not None:
        form.tbu.data = tonal_system.tbu_unit
        form.prosodic_foot.data = get_info_status(tonal_system.prosodic_foot)
        form.prosodic_foot_relevance.data = get_info_status(tonal_system.prosodic_foot_relevance)
        form.tonal_domain_size_min.data = tonal_system.tdminsize
        form.tonal_domain_size_max.data = tonal_system.tdmaxsize
        form.td_boundary.data = tonal_system.td_boundaries
        form.td_proto_unit.data = tonal_system.tdprotounit
        form.td_gt_proto.data = tonal_system.td_gt_proto
        form.td_lt_proto.data = tonal_system.td_lt_proto
        form.tonal_levels.data = tonal_system.numtlevels
        form.tonemic_components.data = tonal_system.tonemic_components
        if tonal_system.grammatical_tones:
            form.has_tonal_morphemes.data = get_info_status(tonal_system.grammatical_tones.has_tonal_morphemes)
            form.tonal_plus_segment.data = get_info_status(tonal_system.grammatical_tones.tonal_plus_segment)
            form.has_floating_tones.data = get_info_status(tonal_system.grammatical_tones.has_floating_tones)
            form.multitoneme_morphemes.data = get_info_status(tonal_system.grammatical_tones.multitoneme_morphemes)
            form.numtones.data = tonal_system.grammatical_tones.numtones
            form.tonal_functions.data = tonal_system.grammatical_tones.tonal_functions
            form.tone_bearers.data = tonal_system.grammatical_tones.tone_bearers
        if tonal_system.orthography:
            form.orthography.data = tonal_system.orthography.tones_marked
    if surface_tones is not None:
        form.downdrift.data = get_info_status(surface_tones.downdrift)
        form.downstep.data = get_info_status(surface_tones.downstep)
        form.nontoned_syllables.data = get_info_status(surface_tones.nontoned_syllables)
        form.tonalization_method.data = surface_tones.tonalization_method
        form.default_tlevel.data = surface_tones.default_tlevel
        form.nontonal_morphemes.data = get_info_status(surface_tones.nontonal_morphemes)
        form.obligatority.data = get_info_status(surface_tones.obligatority)
        form.culminativity.data = get_info_status(surface_tones.culminativity)
        form.stress.data = get_info_status(surface_tones.stress)
        form.stress_td_association.data = get_info_status(surface_tones.stress_td_association)
        form.toneme_syntax_association.data = get_info_status(surface_tones.toneme_syntax_association)


    return render_template('language/update.html', desc=desc,
                           lang_info=lang_info, tonal_system=tonal_system, form=form,
                           tonemes=list(tonemes), components=components, criteria=criteria)


@bp.route('/<lang_id>/edit', methods=('GET', 'POST'))
@login_required
def edit_langinfo(lang_id):
    lang_info = LangInfo.query.get(lang_id)
    thot_lang_info = lang_info.thot or ThotLangInfo(lang_id=lang_id)
    form = ThotLangInfoForm(obj=thot_lang_info)

    if form.validate_on_submit():
        form.populate_obj(thot_lang_info)
        db.session.add(thot_lang_info)
        db.session.commit()
        return redirect(url_for('language.update', lang_id=lang_id))
    elif request.method == 'POST':
        flash(f"Invalid input value: {form.errors}", 'warning')

    return render_template('language/langinfo.html',
                           lang_info=lang_info,
                           form=form)


@bp.route('/<lang_id>/delete', methods=('POST',))
@login_required
def delete(lang_id):
    desc = Descriptions.query.get_or_404(lang_id)
    tonal_system = TonalSystem.query.get(lang_id)
    surface_tones = SurfaceTones.query.get(lang_id)
    db.session.delete(desc)
    if tonal_system:
        db.session.delete(tonal_system)
    if surface_tones:
        db.session.delete(surface_tones)
    db.session.commit()
    return redirect(url_for('language.index'))


@bp.route('/toneme/create/<lang_id>', methods=('GET',))
@login_required
def create_toneme(lang_id):
    toneme = TonemesInventory(lang_id=lang_id)
    db.session.add(toneme)
    db.session.commit()
    return redirect(url_for('language.edit_toneme', id=toneme.toneme_id))


@bp.route('/toneme/<int:id>/delete', methods=('POST',))
@login_required
def delete_toneme(id):
    toneme = db.session.get(TonemesInventory, id)
    db.session.delete(toneme)
    db.session.commit()
    return {
        "toneme_id": id
    }


@bp.route('/allotone/create', methods=('POST',))
@login_required
def create_allotone():
    data = request.json
    print(data)
    allotone = Allotones(toneme_id=data['toneme_id'], name=data['name'])
    db.session.add(allotone)
    for compid, floating in zip(data['components'], data['floats']):
        level = LevelsInventory.query.get(compid)
        allotone = add_allotone_component(allotone, level, floating)
    db.session.commit()
    return redirect(url_for('language.edit_toneme', id=data['toneme_id']))


@bp.route('/allotone/<int:id>/delete', methods=('POST',))
@login_required
def delete_allotone(id):
    allotone = db.session.get(Allotones, id)
    db.session.delete(allotone)
    db.session.commit()
    return {
        "allotone_id": id
    }


@bp.route('/toneme/<int:id>', methods=('GET', 'POST'))
def edit_toneme(id):
    toneme = TonemesInventory.query.get_or_404(id)
    lang_id = toneme.lang_id
    lang_name = LangInfo.query.get(lang_id).name
    components = get_toneme_components(lang_id)
    critdict = {c.criterion: c.comment for c in toneme.criteria}
    npdict = {c.component: c.other for c in toneme.nonpitch}
    criteria = TonemeCriteria.query.all()
    nonpitch = NonpitchFeatures.query.all()

    if request.method == 'POST':
        data = request.json
        toneme.toneme_name = data['toneme_name']
        toneme.default_tone = data['default']
        complist = []
        for order, tlevel in enumerate(data['components'], start=1):
            floating = data['floats'][order-1]
            c = TonemesComposition(toneme_id=toneme.toneme_id, order=order, tlevel=tlevel, floating=floating)
            complist.append(c)
        toneme.components[:] = complist
        critdata = {int(d['id']): d['rationale'] for d in data['criteria']}
        for c in list(toneme.criteria):
            if c.criterion in critdata:
                if not c.comment == critdata[c.criterion]:
                    c.comment = critdata[c.criterion]
                del critdata[c.criterion]
            else:
                toneme.criteria.remove(c)
        for critid, comment in critdata.items():
            crit = TonemeStatus(toneme_id=toneme.toneme_id, criterion=critid, comment=comment)
            toneme.criteria.append(crit)
        npdata = {int(d['id']): d['rationale'] for d in data['nonpitch']}
        for np in list(toneme.nonpitch):
            if np.component in npdata:
                if not np.other == npdata[np.component]:
                    np.other = npdata[np.component]
                del npdata[np.component]
            else:
                toneme.nonpitch.remove(np)
        for npid, comment in npdata.items():
            np = NonpitchComponents(toneme_id=toneme.toneme_id, component=npid, other=comment)
            toneme.nonpitch.append(np)
        db.session.commit()

        return redirect(url_for('language.edit_toneme', id=toneme.toneme_id))

    return render_template('language/toneme.html', lang_name=lang_name,
                           toneme=toneme, components=components,
                           critdict=critdict, npdict=npdict,
                           criteria=criteria, nonpitch=nonpitch)


@bp.route('/rule/create/<lang_id>', methods=('GET',))
@login_required
def create_rule(lang_id):
    rule = TonalRules(lang_id=lang_id)
    db.session.add(rule)
    db.session.commit()
    return redirect(url_for('language.edit_rule', id=rule.rule_id))


@bp.route('/rule/<int:id>/delete', methods=('POST',))
@login_required
def delete_rule(id):
    rule = TonalRules.query.get_or_404(id)
    db.session.delete(rule)
    db.session.commit()
    return {
        "rule_id": id
    }


@bp.route('/rule/<int:id>', methods=('GET', 'POST'))
def edit_rule(id):
    rule = TonalRules.query.get_or_404(id)
    lang_rules = TonalRules.query.filter(TonalRules.lang_id == rule.lang_id).filter(TonalRules.rule_id != rule.rule_id)
    form = TonalRuleForm(obj=rule)
    form.depends_on.query = lang_rules
    
    if g.user is None:
        for f in form:
            setattr(f, 'render_kw', {'disabled':''})

    if g.user is not None:
        if form.validate_on_submit():
            form.populate_obj(rule)
            db.session.add(rule)
            db.session.commit()
            return redirect(url_for('language.update', lang_id=rule.lang_id, _anchor='tonal-rules'))
        elif request.method == 'POST':
            flash(f"Invalid input value: {form.errors}", 'warning')

    return render_template('language/edit_rule.html', lang_id=rule.lang_id, rule=rule, form=form)


@bp.route('/<lang_id>/texts', methods=('GET',))
def texts(lang_id):
    lang_info = LangInfo.query.get_or_404(lang_id)
    texts = TextMetadata.query.filter_by(lang_id=lang_id).all()
    return render_template('language/texts.html', lang_info=lang_info, texts=texts)


@bp.route('/<lang_id>/texts/create', methods=('GET',))
@login_required
def create_text(lang_id):
    text = TextMetadata(lang_id=lang_id, operator_id=g.user.id)
    db.session.add(text)
    db.session.commit()
    return redirect(url_for('language.edit_text', id=text.text_id))


def load_text_data(data):
    Token = namedtuple('Token', 'token markup gloss')
    wb = load_workbook(data)
    ws = wb.worksheets[0]
    tokens = []
    for row in ws.iter_rows(max_col=3):
        tokens.append(Token(*[cell.value or '' for cell in row]))
    return tokens


def replace_text_data(text: TextMetadata, tokens: list):
    if text.tokens:
        text.tokens[:] = []
    for token in tokens:
        to = Tokens(text_id=text.text_id, lang_id=text.lang_id, **token._asdict())
        text.tokens.append(to)
    db.session.add(text)
    db.session.commit()


@bp.route('/text/<int:id>', methods=('GET', 'POST'))
def edit_text(id):
    text = TextMetadata.query.get_or_404(id)
    tonal_system = TonalSystem.query.get_or_404(text.lang_id)
    form = TextUploadForm()
    errors = text.errors
    if g.user is None:
        for f in form:
            setattr(f, 'render_kw', {'disabled':''})

    if g.user is not None:
        if form.validate_on_submit():
            text.reference = form.reference.data
            text.author = form.author.data
            text.date = form.date.data
            text.genre = form.genre.data
            text.url = form.url.data
            text.operator_id = g.user.id
            if form.srcfile.data:
                srcfile = form.srcfile.data
                filename = secure_filename(srcfile.filename)
                text.filename = filename
                tokens = load_text_data(srcfile)
                tonemes = [str(t) for t in tonal_system.tonemes]
                errors = markup_errors(tokens, tonemes)
                if errors:
                    for e in errors:
                        flash(e.message, 'danger')
                else:
                    replace_text_data(text, tokens)
            db.session.add(text)
            db.session.commit()
            if not errors:
                return redirect(url_for('language.texts', lang_id=text.lang_id))

        elif request.method == 'POST':
            flash(f"Invalid input value: {form.errors}", 'warning')

    if errors:
        for e in errors:
            flash(e.message, 'danger')

    form.reference.data = text.reference
    form.author.data = text.author
    form.date.data = text.date
    form.genre.data = text.genre
    form.url.data = text.url
    return render_template('language/edit_text.html', text=text, form=form)


@bp.route('/text/<int:id>/delete', methods=('POST',))
@login_required
def delete_text(id):
    text = TextMetadata.query.get_or_404(id)
    db.session.delete(text)
    db.session.commit()
    return {
        "text_id": id
    }
